<?php

namespace App\Exceptions;

use Exception;

class ForbiddenException extends Exception
{
    public function render(){
        return view('errors.forbidden');
    }
}
