<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\BillsCategory;
use App\Models\Business;
use App\Models\BusinessSetting;
use App\Models\Company;
use App\Models\SalesCategory;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function businessInitialization($company, $user) {

        $client = new Company();
        
        $client->name = "Publico en General";
        $client->rfc = "XAXX010101000";
        $client->company_type = "client";
        $client->active = true;
        $client->company_id = $company->id;
        $client->blocked = true;
        $client->created_by = $user->id;

        $client->save();


        $provider = new Company();
        
        $provider->name = "Publico en General";
        $provider->rfc = "XAXX010101000";
        $provider->company_type = "provider";
        $provider->active = true;
        $provider->company_id = $company->id;
        $provider->blocked = true;
        $provider->created_by = $user->id;

        $provider->save();

        $icategory = new SalesCategory();
        $icategory->name = "General";
        $icategory->company_id = $company->id;
        $icategory->created_by = $user->id;
        $icategory->blocked = true;
        $icategory->save();

        $ecategory = new BillsCategory();
        $ecategory->name = "General";
        $ecategory->company_id = $company->id;
        $ecategory->created_by = $user->id;
        $ecategory->blocked = true;
        $ecategory->save();

        $sales = new BusinessSetting();

        $sales->company_id = $company->id;
        $sales->meta_key = "last_sales_folio";
        $sales->meta_value = 1;

        $sales->save();

        $bills = new BusinessSetting();

        $bills->company_id = $company->id;
        $bills->meta_key = "last_bills_folio";
        $bills->meta_value = 1;

        $bills->save();


    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $company = Business::create([
            'name' => $request->company_name,
            'email' => $request->email,
            'type' => 1,
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'company_id' => $company->id,
            'is_admin' => true
        ]);

        $this->businessInitialization($company, $user);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
