<?php

namespace App\Http\Controllers;

use App\Http\Requests\billsPostRequest;
use App\Models\Bill;
use App\Models\BillPayment;
use App\Models\BillsCategory;
use App\Models\BusinessSetting;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spipu\Html2Pdf\Html2Pdf;
use App\Managers\BillManager;
class BillsController extends Controller
{
    public $billManager;
    public function __construct()
    {
        $this->billManager = new BillManager();
    }
    public function getTransactionsByType($type, $id){
        $this->check_permissions('sales_access');

        $bills= $this->business()->bills()->where('providers',$id)->orderByDesc('date')->paginate(10);
        $bills = $this->billManager->formatBills($bills);
        $results = collect(["transactions" => $bills, "total" => $bills->sum('total')]);
        return $results;
    }

    public function getBills(){
        $this->check_permissions('bills_access');
        $bills = $this->business()->bills()->orderByDesc('date')->take(30)->get();

        $bills = $this->billManager->formatBills($bills);

       return view('bills.index', [
            'bills' => $bills,
            "total" => $bills->sum('total'),
            "providers" => Company::providers()->get(),
            "categories" => $this->business()->billsCategories
       ]);
    }
    public function getBillsReportIndex(){
        $this->check_permissions('reports_bills');

        return view('reports.bills');
     }

    public function getBill($id = null){
        $this->check_permissions('bills_access');
        $bill = [];

        if(!empty($id)){
            $bill = $this->business()->bills->find($id);
            $bill = $this->billManager->formatBills($bill);
        }

        return view("bills.bills-form", [
        'bill' => $bill, 
        'categories' => $this->business()->billsCategories,
        'providers' => Company::providers()->get(),
        'folio' => $this->business()->businessSettings()->where('meta_key', 'last_bills_folio')->first()->meta_value

    
    ]);
    }

    public function saveBill(billsPostRequest $request){
       
        $permission = $request->id == null ? "bills_create" : "bills_edit";

        $this->check_permissions($permission);

        $model =  new Bill();
        if(!empty($request->id)){
            $model = $this->business()->bills->find($request->id);
        }

        $model->reference = $request->reference;
        $model->date = $request->date;
        $model->description = $request->description;
        $model->categories = $request->categories;
        $model->providers = $request->providers;

        $model->paid = $request->paid  == "on";
        if($request->id == null){
            $model->folio = $this->business()->businessSettings->where('meta_key', 'last_bills_folio')->first()->meta_value;
            $this->business()->businessSettings()->where('meta_key', 'last_bills_folio')->increment('meta_value');
        }
        $bill = $this->business()->bills()->save($model);
        
        //verificar si se removera este tipo de payments
        $this->saveBillsPaymentsByRequest($request, $bill);

        return redirect()->route('update-bill', ['id' => $bill->id])->with('success', true);    
    }

    public function saveBillsPaymentsByRequest($request, $bill){
        $cash = $request->id == null ? new BillPayment() : $this->first(BillPayment::where('bill_id', $bill->id)->where('payment_type', 'cash'));
        if(!empty($cash)){
            $cash->bill_id = $bill->id;
            $cash->payment_type = "cash";
            $cash->amount = $request->cash ? str_replace(',' , '' ,$request->cash) : 0;
            $this->save($cash);
        }else {
            $cash = new BillPayment();
            $cash->bill_id = $bill->id;
            $cash->payment_type = "cash";
            $cash->amount = $request->cash ? str_replace(',' , '' ,$request->cash) : 0;
            $this->save($cash);
        }

        $card = $request->id == null ? new BillPayment() : $this->first(BillPayment::where('bill_id', $bill->id)->where('payment_type', 'card'));
        if(!empty($card)){
            $card->bill_id = $bill->id;
            $card->payment_type = "card";
            $card->amount = $request->card ? str_replace(',' , '' ,$request->card) : 0;
            $this->save($card);
        }else{
            $card = new BillPayment();
            $card->bill_id = $bill->id;
            $card->payment_type = "card";
            $card->amount = $request->card ? str_replace(',' , '' ,$request->card) : 0;
            $this->save($card);
        }
    }
    public function deleteBill(Request $request){
        $this->check_permissions('bills_delete');

        $id = $request['id'];
        
        $bill = $this->business()->bills()->where('id', $id)->first();

        if($bill){
            $bill->delete();
            return $this->success([],"Eliminado correcamente");
        }

        return $this->fail("Compañia no encontrado");
       
    }

    public function getBillsByDate(Request $request){
        $reference = $request['reference'];
        $bills= $this->business()->bills();

        //si tiene referencia
        if(!empty($reference)){

            $bills = $bills->where(
                function($query) use($reference) {
                  return $query
                         ->where('reference', 'LIKE', '%'. $reference .'%')
                         ->orWhere('folio',  intval(preg_replace("/[^0-9]/", "", $reference )));
                 })->orderByDesc('created_at');
        }else {
         
            //si tiene fechas
            if(!is_null($request['start']) || !is_null($request['end'])){
                $start = new Carbon($request['start']);
                $end = new Carbon($request['end']);
                $bills->whereBetween('date', [$start->startOfDay(), $end->endOfDay()]);
            }

            //si tiene proovedor
            if(!empty(request()->input('provider'))){
                $bills->where('providers', request()->input('provider'));
            }

            //si tiene categoria
            if(!empty(request()->input('category'))){
                $category = request()->input('category');
                $bills->whereRaw("find_in_set($category,categories)");
            }
            
        }

        
        $bills = $this->billManager->formatBills($bills->get());
        return $this->success([
            "bills" => $bills,
            "total" => $bills->sum('total')
        ]);



    }

    public function getBillsProviders(Request $request){

        $start = new Carbon($request['start']);
        $end = new Carbon($request['end']);
        $bills = $this->business()->bills()->whereBetween('date', [$start->startOfDay(), $end->endOfDay()])->get();
        $bills = $this->billManager->formatBills($bills);
          
        $model = collect();
        foreach (Company::providers()->get() as $key => $provider) {
            $modelTemp = collect();
            $billsCollection =  $bills->filter(function($collection) use($provider){ 
                return in_array($provider->id,  explode(',',$collection->providers),); 
              });
            $modelTemp['provider'] = $provider->name;
            $modelTemp['bills'] = count($billsCollection);
            $modelTemp['billsTotal'] = $billsCollection->sum('total');
            $modelTemp['lastBill'] = empty($billsCollection->sortByDesc('date')->first()) ? '' : $billsCollection->sortByDesc('date')->first()->date->format('d-m-Y');
            $model->push($modelTemp);
        }
       
        return $this->success([
            "bills" => $model,
        ]);


    }

    public function getBillsCategories(Request $request){

        $start = new Carbon($request['start']);
        $end = new Carbon($request['end']);
        $bills = $this->business()->bills()->whereBetween('date', [$start->startOfDay(), $end->endOfDay()])->get();
        $bills = $this->billManager->formatBills($bills);
          
        $model = collect();
        foreach ($this->business()->billsCategories()->get() as $key => $provider) {
            $modelTemp = collect();
            $billsCollection =  $bills->filter(function($collection) use($provider){ 
                return in_array($provider->id,  explode(',',$collection->categories),); 
              });
            $modelTemp['category'] = $provider->name;
            $modelTemp['bills'] = count($billsCollection);
            $modelTemp['billsTotal'] = $billsCollection->sum('total');
            $modelTemp['lastBill'] = empty($billsCollection->sortByDesc('date')->first()) ? '' : $billsCollection->sortByDesc('date')->first()->date->format('d-m-Y');
            $model->push($modelTemp);
        }
       
        return $this->success([
            "bills" => $model,
        ]);

    }

    public function getReportsBills(Request $request){

        $type = $request['type'];
        if($type == "general"){
            return $this->getBillsByDate($request);
        }
        if($type == "top-providers"){
            return $this->getBillsProviders($request);
        }
        if($type == "top-categories"){
            return $this->getBillsCategories($request);
        }

    }


    public function viewBillPDF($id){
        $bill = $this->business()->bills()->find($id);
        $html2pdf = new Html2Pdf();
        $html = view('bills.bill-pdf', ['bill' => $bill]);
        $html2pdf->writeHTML($html);
        return $html2pdf->output();
    }
}
