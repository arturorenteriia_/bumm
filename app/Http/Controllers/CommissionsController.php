<?php

namespace App\Http\Controllers;

use App\Managers\SaleManager;
use App\Models\Commission;
use App\Models\Employee;
use App\Models\Sale;
use App\Models\SalesCategory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommissionsController extends Controller
{
    public function index(){
        $this->check_permissions(['reports_commissions', 'reports_commissions_own']);

        if(has_permission('reports_commissions')){
            $employees = $this->business()->employees()->get();
            $employees->prepend(["name" => "Reporte", "last_name" => "General", "id" => '0']);
        }else{
            $employees = $this->business()->employees()->where('id', Auth::user()->employee_id)->get();
        }

        return view('reports.commissions', ['employees' => $employees]);
     }

     public function getReportsCommissions(Request $request){
        $start = new Carbon($request['start']);
        $end = new Carbon($request['end']);
        $controller = new SaleManager();


        if($request['id'] == '0'){
            $commissions =  Commission::sales()->with(['sale', 'employee'])
            ->whereHas('sale', function (Builder $query) use($start, $end){
                $query->whereBetween('date', [$start->startOfDay(), $end->endOfDay()]);
            })->get();
        }else{
            $commissions =  Commission::sales()->with(['sale', 'employee'])
            ->whereHas('sale', function (Builder $query) use($start, $end){
                $query->whereBetween('date', [$start->startOfDay(), $end->endOfDay()]);
            })->where('employee_id', $request['id'])->get();
        }
        $total = 0;
        foreach ($commissions as $key => $commission) {
            $commission['total'] = $controller->getPaymentsTotal($commission->sale);
            $commission['categories_names'] = $controller->getNamesByID($commission->sale->categories, '\SalesCategory');
            $commission['clients_names'] = $controller->getNamesByID($commission->sale->clients, '\Company');
            $total += $commission['total'];
            $commission['free_text'] = $commission->free ? "Si" : "No";
        }

        return $this->success([
            "sales" => $commissions,
            "resume" =>["totalCommissions" => $commissions->sum('amount')]
        ]);
        
     }

   
     public function getInfoChartCommissions(Request $request){
        $dataSet = [0];

        for ($i=1; $i<13 ; $i++) {
               
            $date = Carbon::now();
            $date->copy()->startOfWeek(Carbon::MONDAY);
            $date->copy()->endOfWeek(Carbon::SUNDAY);

            $date->setDate($request['year'],$i, 1);
            $weekStart =  $date->copy()->startOfMonth();
            $weekEnd =  $date->copy()->endOfMonth();
            $type = $request['type'];

            if($request['type'] == '0'){
                $commissions =  Commission::sales()->with(['sale', 'employee'])
                ->whereHas('sale', function (Builder $query) use($weekStart, $weekEnd, $type){
                    $query->whereBetween('date', [$weekStart->startOfDay(), $weekEnd->endOfDay()]);
                })->where('employee_id', '>', 0)->get();
            }else{
                $commissions =  Commission::sales()->with(['sale', 'employee'])
                ->whereHas('sale', function (Builder $query) use($weekStart, $weekEnd, $type){
                    $query->whereBetween('date', [$weekStart->startOfDay(), $weekEnd->endOfDay()]);
                })->where('employee_id', $type)->get();
            }
            $total = 0;
            foreach ($commissions as $key => $commission) {
                $total = $total + $commission->amount;
            }
            $dataSet[] = $total;
        }

        $labels = ['','Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        return $this->success([
            "datasets" => $dataSet,
            "labels" => $labels,
            "total" => array_sum($dataSet),
        ]);
    }

     
}
