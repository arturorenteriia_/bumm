<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompaniesPostRequest;
use App\Models\Bill;
use App\Models\BillsCategory;
use App\Models\Business;
use App\Models\Company;
use App\Models\Sale;
use App\Models\SalesCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    public function indexClients (){
        $this->check_permissions('clients_access');
        $clients = Company::clients()->get();
        return view('clients.index', [
            'clients' => $clients
        ]);
    }
    public function getModelByName(Request $request){

        $model = $this->business()->companies()->where(
            function($query) use($request) {
              return $query
                     ->where('name', 'LIKE', '%'. $request['name'] .'%')
                     ->orWhere('rfc', 'LIKE', '%'. $request['name'] .'%')
                     ->orWhere('legal_name', 'LIKE', '%'. $request['name'] .'%');
             })
        ->where('company_type', $request['type'])->orderByDesc('created_at')->get();
        return $this->success($model);
    }
    public function indexProviders (){
        $this->check_permissions('providers_access');

        $providers = Company::providers()->get();
        return view('providers.index', [
            'providers' => $providers
        ]);
    }

    public function saveCompany(CompaniesPostRequest $request){
        $type = $request->company_type == "client" ? "clients" : "providers";

        $rule = $request->id == null ? "create" : "edit";
        
        $this->check_permissions($type.'_'.$rule);

        $model = $request->id == null ? new Company() : $this->business()->companies()->find($request->id);

        if($rule == "edit" && $model->blocked == true){
            return redirect()->route('update-'.$model->company_type, ['id' => $model->id])->with('cant-edit', true);
        }
        $model->name = $request->name;
        $model->address = $request->address;
        $model->city = $request->city;
        $model->phone = $request->phone;
        $model->email = $request->email;
        $model->zipcode = $request->zipcode;
        $model->state_id = $request->state_id;
        $model->country_id = 1;
        $model->rfc = $request->rfc;
        $model->legal_name = $request->legal_name;
        $model->active = $request->active == "on" ? true : false;
        $model->company_type = $request->company_type;
        $model->legal_type = $request->legal_type;
        $model->notes = $request->notes;

        $saved = $this->business()->companies()->save($model);

        if($request->isAjax){
            return  $this->success($this->business()->companies()->get());
        }

        return redirect()->route('update-'.$request->company_type, ['id' => $saved->id])->with('success', true);
    }

    

    public function getClient($id){
        $client =  Company::clients()->find($id);
        if(is_null($client)){
            abort(404, 'Page not found');
        }
        return view('clients.clientsForm', [
            "client" => $client, 
        ]);
    }

    public function getProvider($id){
        $provider =   Company::providers()->find($id);
        if(is_null($provider)){
            abort(404, 'Page not found');
        }
        
        return view('providers.providers-form', [
            "provider" => $provider
        ]);
    }

    public function reasingCompany($type , $companyID){
        
        $type = $type == "clients" ? "client" : "provider";
        $category = $this->business()->companies()->where('name', 'Publico En General')->where('company_type', $type)->first();
        
        if($type == "client"){
            $transactions = $this->business()->sales()->where('clients', $companyID)->get();
            foreach ($transactions as $value) {
                $value->clients = $category->id;
                $value->save();
            }
        }

        if($type == "provider"){
            $transactions = $this->business()->bills()->where('providers', $companyID)->get();
            foreach ($transactions as $value) {
                $value->providers = $category->id;
                $value->save();
            }
        }
    }

    public function deleteCompany(Request $request){

        $this->check_permissions($request['type'].'_delete');
        $id = $request['id'];
        
        $company = $this->business()->companies()->where('id', $id)->first();

        if($company){

            if( $company->blocked == true){
                return $this->fail([],"No se puede eliminar porque es generado por el sistema.");
            }

            $this->reasingCompany($request['type'], $company->id);
            $company->delete();
            
            return $this->success([],"Eliminado correcamente");

        }

        return $this->fail([],"Compañia no encontrado");
       
    }

    public function getTransactions(Request $request){

        $type = $request->input('type');
        $id = $request->input('id');

        if($type == "clients"){
            $controller = new SalesController();
        }

        if($type == "providers"){
            $controller = new BillsController();
        }

        $transactions = $controller->getTransactionsByType($type, $id);
        

        return $this->success($transactions,"Eliminado correcamente");

       
    }
}
