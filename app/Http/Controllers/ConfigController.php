<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\BillsCategory;
use App\Models\Sale;
use App\Models\SalesCategory;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;

class ConfigController extends Controller
{
  
    public function saveSalesCategories (Request $request){

        try{
            $category = $request['id'] == null ? new SalesCategory() : SalesCategory::find($request['id']);
            $cleanName = ucwords(strtoLOWER($request['name']));
           
            $category->name = $cleanName;
            $this->business()->salesCategories()->save($category);
            return  $this->success($this->business()->salesCategories()->get());

        }catch(Exception $error){
            return $this->fail($error->getMessage());
        }

    }

    public function saveBillsCategories (Request $request){

        try{
            $category = $request['id'] == null ? new BillsCategory() : BillsCategory::find($request['id']);
            $cleanName = ucwords(strtoLOWER($request['name']));
           
            $category->name = $cleanName;
            $this->business()->billsCategories()->save($category);
            
            return  $this->success($this->business()->billsCategories()->get());

        }catch(Exception $error){
            return $this->fail($error->getMessage());
        }

    }

    public function getSalesCategories (){
        $this->check_permissions('config_sales_access');
        try{
           $categories = $this->business()->salesCategories()->get();
            return view('config.sales', ["categories" => $categories]);

        }catch(Exception $error){
            return $this->fail($error->getMessage());
        }

    }
    public function reasingCategory($category, $type){
        
        $sales = $type == "sale" ? $this->business()->sales()->whereRaw("find_in_set($category->id,categories)")->get() :
        $this->business()->bills()->whereRaw("find_in_set($category->id,categories)")->get();

          $newcategory = $type == "sale" ? $this->business()->salesCategories()->where('name', 'General')->first() :
          $this->business()->billsCategories()->where('name', 'General')->first();

          foreach ($sales as $sale) {
            $categoriesArr = explode(',',$sale->categories); 
            $newArrayCategories = [];
            foreach ($categoriesArr as $key => $cat) {
              if($cat == $category->id){
                $cat = $newcategory->id;
              }else{
                  array_push($newArrayCategories, $cat);
              }
            }
            array_push($newArrayCategories,  $newcategory->id);
            $sale->categories = implode(",", $newArrayCategories);
            $sale->save();
          }

    }
    public function deleteSalesCategory (Request $request){

        try{
          $category = SalesCategory::find($request['id']);
          if($category){
            $this->reasingCategory($category, "sale");
            $category->delete();
          }
          return $this->success( $this->business()->salesCategories()->get(), "Categoria eliminada correctamente");
        }catch(Exception $error){
            return $this->fail($error->getMessage());
        }

    }

    public function deleteBillsCategory (Request $request){

        try{
          $category = BillsCategory::find($request['id']);
          
          if($category){
            $this->reasingCategory($category, "bill");
            $category->delete();
          }
          return $this->success( $this->business()->billsCategories()->get(), "Categoria eliminada correctamente");
        }catch(Exception $error){
            return $this->fail($error->getMessage());
        }

    }
    public function getBillsCategories (){
        $this->check_permissions('config_bills_access');

        try{
           $categories = $this->business()->billsCategories()->get();
            return view('config.bills', ["categories" => $categories]);

        }catch(Exception $error){
            return $this->fail($error->getMessage());
        }

    }

    public function saveBusiness (Request $request){
        $this->check_permissions('config_business_save');

        $this->business()->name = $request->input('name');
        $this->business()->save();
        return redirect()->route('company-profile')->with('success', true);   
    }

}
