<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $total;
    public function check_permissions($permission){
        if(!has_permission($permission)){
            throw new ForbiddenException;
        }
    }

    public function business (){
        return Auth()->user()->business;
    }

    public function companyID(){
        return Auth::user()->company_id;
    }

    public function userID(){
        return Auth::user()->id;
    }

    public function success ($data = [], $message = "Solicitud exitosa"){
        return [
            "success" => true,
            "data"=> $data,
            "message" => $message
        ];
    }
    public function fail ($error = [], $message = "Error"){
        return [
            "success" => false,
            "error"=> $error,
            "message" => $message
        ];
    }

    public function save($model){

        if(!empty($model->id)){
            return $this->update($model);
        }
        $model->created_by = $this->userID();
        $model->company_id = $this->companyID();

        $model->save();
        return $model;
    }

    public function update($model){

        $model->updated_by = $this->userID();
        $model->save();

        return $model;

    }

    public function get($model){
        return $model->where('company_id', $this->companyID())->get();
    }

    public function increment($model, $field){
        return $model->where('company_id', $this->companyID())->increment($field);
    }

    public function paginate($model, $number){
        return $model->where('company_id', $this->companyID())->paginate($number);
    }

    public function first($model){
        return $model->where('company_id', $this->companyID())->first();
    }
}
