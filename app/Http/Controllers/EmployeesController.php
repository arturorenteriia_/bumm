<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeePostRequest;
use App\Models\Employee;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeesController extends Controller
{
    public function getEmployees(){
        $this->check_permissions('config_employees_access');

        $employees =  $this->business()->employees()->orderByDesc('created_at')->orderByDesc('created_at')->get();

        return view('config.employees', [
            "employees" => $employees
        ]);
    }

    public function getModelByName(Request $request){

        $model =  $this->business()->employees()->where(
            function($query) use($request) {
              return $query
                     ->where('name', 'LIKE', '%'. $request['name'] .'%')
                     ->orWhere('last_name', 'LIKE', '%'. $request['name'] .'%')
                     ->orWhere('email', 'LIKE', '%'. $request['name'] .'%')
                     ->orWhere('phone', 'LIKE', '%'. $request['name'] .'%');
             })->orderByDesc('created_at')->get();
        return $this->success($model);
    }


    public function getEmployee($id){
        $this->check_permissions('config_employees_access');
        $this->check_permissions('config_access');

        $employee =  $this->business()->employees()->where('id', $id)->first();
        if(is_null($employee)){
            abort(404, 'Page not found');
        }

        if(!empty($employee->user)){
            $employee['permissions'] = $employee->user->permissions;
        }
        
        return view('config.new-employee', [
            "employee" => $employee
        ]);
    }

    public function saveEmployee(EmployeePostRequest $request){

            $id = $request->id;
 
            $model = is_null($id) ? new Employee() : $this->business()->employees()->where('id', $id)->first();
            $model->name = $request->name;
            $model->last_name = $request->last_name;
            $model->address = $request->address;
            $model->city = $request->city;
            $model->phone = $request->phone;
            $model->email = $request->email;
            $model->zipcode = $request->zipcode;
            $model->state_id = $request->state_id;
            $model->country_id = 1;
            $model->birth = $request->birth;
            $model->account = $request->account == "on" ? true : null;
            $model->active = $request->active == "on" ? true : false;
            $model->commission = $request->commission;
            $model->salary = $request->salary;
            $model->payment_type = $request->payment_type;

            $saved = $this->business()->employees()->save($model);
            if($saved->account == 1){
                $this->createUserAccount($request, $saved->id);
            }

            if(!empty($request['permissions'])){
                $user =  $this->business()->users()->where('employee_id', $saved->id)->first();

                if($user){
                    $user->permissions = $request['permissions'];
                    $user->save();
                }

            }

            if(is_null($id)){
                redirect()->route('update-employee', ['id' => $saved->id])->with('success', true);
            }

            return redirect()->route('update-employee', ['id' => $saved->id])->with('success', true);
            
      
        
    }
    public function createUserAccount($request, $employeeID){
        
        $user =  User::where('employee_id', $employeeID)->first();
        if(empty($user)){
            $user = new User();
        }
        $user->name = $request->name;
        $user->email = $request->username;
        if(!empty($request->password)){
            $user->password =  Hash::make($request->password);
            $user->employee_id = $employeeID;
        }
        
        $user->is_admin =  false;
        $user->super_user =  false;
        
        $this->save($user);

        return true;
    }

    public function deleteEmployee(Request $request){

        $id = $request['id'];
        
        $employee = $this->business()->employees()->where('id', $id)->first();

        if($employee){
            $employee->user()->delete();
            $employee->delete();
            
            return $this->success([],"Eliminado correcamente");

        }

        return $this->fail("Empleado no encontrado");
       
    }
}
