<?php

namespace App\Http\Controllers;

use App\Managers\SaleManager;
use App\Models\Bill;
use App\Models\Business;
use App\Models\BusinessSetting;
use App\Models\Sale;
use App\Models\SalesCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GeneralController extends Controller
{
    public function uploadProfilePic( Request $request){
        $image = $request->files;
        foreach ($image as  $value) {
            foreach ($value as $value2) {
                $name = $this->business()->id.'--'.$value2->getClientOriginalName();
                $value2->move(public_path('images/profile-pics'),$name);
                $this->business()->pic = $name;
                $this->business()->save();
             }
        }
        return $this->success($value2->getClientOriginalName());
    }
    public function getInfoChartTotals(Request $request){
         switch ($request['range']) {
            case 'monthly':
                $data = $this->getTotalsByDayMonth($request['year'], $request['month'], $request['sale']);
                break;
            case 'yearly':
                $data = $this->getTotalsByMonthYear($request['year'], $request['sale']);
                break;
            case 'weekly':
                $data = $this->geTotalsByDayWeek($request['year'], $request['month'], $request['day'], $request['sale']);
                break;
            
            default:
                $data = [];
                break;
         }
        return $this->success($data);
    }
    
    public function getTotalsByMonthYear($year, $isSale){
        $manager = new SaleManager();

        $dataSet = [0];
        for ($i=1; $i<13 ; $i++) {
               
            $date = Carbon::now();
            $date->copy()->startOfWeek(Carbon::MONDAY);
            $date->copy()->endOfWeek(Carbon::SUNDAY);

            $date->setDate($year,$i, 1);
            $weekStart =  $date->copy()->startOfMonth();
            $weekEnd =  $date->copy()->endOfMonth();
            
            if(is_null($isSale)){ 
                $models  = $this->business()->bills()->whereBetween('date', [$weekStart, $weekEnd])
                ->get();
                
            }else{
                $models  = Sale::sales()->whereBetween('date', [$weekStart, $weekEnd])
                ->where('status', '<>', $manager::FREE)->get();
            }
            
            $total = 0;

            foreach ($models as $m) {
                $total = $total + $manager->getPaymentsTotal($m);
            }

            $dataSet[] = $total;
        }

        $labels = ['','Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        return[
            "datasets" => $dataSet,
            "labels" => $labels,
            "total" => array_sum($dataSet),
        ];
    }
    public function getTotalsByDayMonth($year, $month, $isSale){
        $manager = new SaleManager();
        $dataSet = [0];

        $date = Carbon::now();
        $date->setDate($year,$month , 1);
        $numbersOfDays = $date->daysInMonth;
        $labels = [''];
        for ($i=1; $i<$numbersOfDays +1 ; $i++) {
               
            $labels[] = $i;
            $date->copy()->startOfWeek(Carbon::MONDAY);
            $date->copy()->endOfWeek(Carbon::SUNDAY);

            $date->setDate($year,$month , $i);
            
            $dayStart =  $date->copy()->startOfDay();
            $dayEnd =  $date->copy()->endOfDay();

            if(is_null($isSale)){ 
                $data = $this->business()->bills()->whereBetween('date', [$dayStart, $dayEnd])->get();
                
            }else{
                $data = Sale::sales()->whereBetween('date', [$dayStart, $dayEnd])
            ->where('status', '<>', $manager::FREE)->get();
            }
            $total = 0;

            foreach ($data as $d) {
                $total = $total +   $manager->getPaymentsTotal($d);
            }

            $dataSet[] = $total;

        }

        return[
            "datasets" => $dataSet,
            "labels" => $labels,
            "total" => array_sum($dataSet),
        ];
    }

    public function geTotalsByDayWeek($year, $month, $day, $isSale){
        $manager = new SaleManager();
        $dataSet = [0];
        $labels[] = [''];
        $date = Carbon::now();
        for ($i=0; $i<6+1 ; $i++) {
            $date->copy()->startOfWeek(Carbon::MONDAY);
            $date->copy()->endOfWeek(Carbon::SUNDAY);

            $date->setDate($year,$month , $day);
          
            $dayStart = $date->copy()->startOfWeek()->addDays($i)->startOfDay();
            $dayEnd = $date->copy()->startOfWeek()->addDays($i)->endOfDay();
          
            if(is_null($isSale)){ 
                $data =$this->business()->bills()->whereBetween('date', [$dayStart, $dayEnd])->get();
            }else{
                $data =Sale::sales()->whereBetween('date', [$dayStart, $dayEnd])
                ->where('status', '<>', $manager::FREE)->get();
            }


            $days = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'];
            $labels[] = $days[$dayStart->formatLocalized('%w')]. '-' .$dayStart->format('d');

            $total = 0;

            foreach ($data as  $d) {
                $total = $total + $manager->getPaymentsTotal($d);
            }
            $dataSet[] = $total;
            
        }
        
        return[
            "datasets" => $dataSet,
            "labels" => $labels,
            "total" => array_sum($dataSet),
        ];
    }
    public function fixupdate(){
        $sales = Sale::all();
        
        foreach ($sales as $key => $value) {
            $value->status = "PAID";

            $value->save();
        }

        return "ok";
    }
}
