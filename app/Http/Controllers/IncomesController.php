<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IncomesController extends Controller
{
    public function index(){
        $this->check_permissions('reports_incomes');

        return view('reports.incomes');
     }

     public function getInfoChartIncomes(Request $request){
        $dataSet = [0];
       $incomes=  $this->getReportsIncomesBymonth($request, true);

       foreach ($incomes as $key => $in) {
        $dataSet[] = $in['incomes'];
       }

       

        $labels = ['','Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        return $this->success([
            "datasets" => $dataSet,
            "labels" => $labels,
            "total" => array_sum($dataSet),
        ]);
    }

     public function getPaymentsTotal($model, $isBill = false){
         $total = 0;
         foreach ($model as $key => $m) {
           if($isBill){
                $total += $m->payments->sum('amount') * (($m->taxes_amount / 100) + 1);
           }else{
            if(!$m->free ){
                $total += $m->payments->sum('amount') * (($m->taxes_amount / 100) + 1);
            }
           }
         }
       
        return $total;
    }
    public function getReportsIncomesBymonth(Request $request, $justIncomes = false)
    {
        setlocale(LC_ALL, 'es_ES');
        $dateToStart = Carbon::createFromFormat('Y/m/d H:i:s',  $request['year'].'/01/01 00:00:00');

        $dataSet = [];
        
            for ($i=1; $i<13 ; $i++) {

                $date = Carbon::now();
                $date->copy()->setWeekStartsAt(Carbon::MONDAY);
                $date->copy()->setWeekEndsAt(Carbon::SUNDAY);
                $date->setDate($request['year'],$i, 1);
                $startingDate =  $date->copy()->startOfMonth();
                $endingDate =  $date->copy()->endOfMonth();

                $sales = Sale::sales()->whereBetween('date', [$startingDate, $endingDate])->get();
                $bills = $this->business()->bills()->whereBetween('date', [$startingDate, $endingDate])->get();
                $salesTotal = $this->getPaymentsTotal($sales);
                $billsTotal = $this->getPaymentsTotal($bills);  
               
                $dataSet[] = [
                        'date' => $startingDate->formatLocalized('%B'),
                        'dateStart' =>  Carbon::parse($startingDate->toDateString())->format('d-m-Y'),
                        'dateEnd' =>Carbon::parse($endingDate->toDateString())->format('d-m-Y'),
                        'sales'=> $salesTotal,
                        'bills'=> $billsTotal,
                        'incomes' => $salesTotal - $billsTotal
                    ];
            
        }
        if($justIncomes){
            return $dataSet;
        }
        return $dataSet;
    }

    public function getReportsIncomesByYear(Request $request)
    {
        setlocale(LC_ALL, 'es_ES');
        $dateToStart = Carbon::createFromFormat('Y/m/d H:i:s',  $request['year'].'/01/01 00:00:00');

        $dataSet = [];
        

                $date = Carbon::now();
                $date->copy()->setWeekStartsAt(Carbon::MONDAY);
                $date->copy()->setWeekEndsAt(Carbon::SUNDAY);
                $date->setDate($request['year'],1, 1);
                $startingDate =  $date->copy()->startOfYear();
                $endingDate =  $date->copy()->endOfYear();

                $sales = Sale::sales()->whereBetween('date', [$startingDate, $endingDate])->get();
                $bills = $this->business()->bills()->whereBetween('date', [$startingDate, $endingDate])->get();
                $salesTotal = $this->getPaymentsTotal($sales);
                $billsTotal = $this->getPaymentsTotal($bills);  
               
                $dataSet[] = [
                        'date' => $startingDate->formatLocalized('%B'),
                        'dateStart' =>  Carbon::parse($startingDate->toDateString())->format('d-m-Y'),
                        'dateEnd' =>Carbon::parse($endingDate->toDateString())->format('d-m-Y'),
                        'sales'=> $salesTotal,
                        'bills'=> $billsTotal,
                        'incomes' => $salesTotal - $billsTotal
                    ];
            
        

        return $dataSet;
    }

    public function getReportsIncomesByWeek(Request $request)
    {
        setlocale(LC_ALL, 'es_ES');
        $dateToStart = Carbon::createFromFormat('Y/m/d H:i:s',  $request['year'].'/01/01 00:00:00');

        $dataSet = [];
        
            for ($i=1; $i<53 ; $i++) {

                $date = Carbon::now();
                $date->copy()->setWeekStartsAt(Carbon::MONDAY);
                $date->copy()->setWeekEndsAt(Carbon::SUNDAY);
                $date->setISODate($request['year'],$i);
                $startingDate =  $date->copy()->startOfWeek();
                $endingDate =  $date->copy()->endOfWeek();

                $sales = Sale::sales()->whereBetween('date', [$startingDate, $endingDate])->get();
                $bills = $this->business()->bills()->whereBetween('date', [$startingDate, $endingDate])->get();
                $salesTotal = $this->getPaymentsTotal($sales);
                $billsTotal = $this->getPaymentsTotal($bills);  
               
                $dataSet[] = [
                        'date' => $startingDate->formatLocalized('%B'),
                        'dateStart' =>  Carbon::parse($startingDate->toDateString())->format('d-m-Y'),
                        'dateEnd' =>Carbon::parse($endingDate->toDateString())->format('d-m-Y'),
                        'sales'=> $salesTotal,
                        'bills'=> $billsTotal,
                        'incomes' => $salesTotal - $billsTotal,
                        'week' => $i
                    ];
            
        }

        return $dataSet;
    }

     public function getReportsIncomes(Request $request){

        if ( $request['type'] == "monthly") {
            $dataSet = $this->getReportsIncomesByMonth($request);
        }

        if ( $request['type'] == "weekly") {
            $dataSet = $this->getReportsIncomesByWeek($request);
        }
        
        if ( $request['type'] == "yearly") {
            $dataSet = $this->getReportsIncomesByYear($request);
        }

        return $this->success($dataSet);
       
     }
}
