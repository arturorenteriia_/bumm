<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    public function index (){
            $this->check_permissions('products');
            try{
               $products = $this->business()->products()->get();
                return view('sales.products', ["products" => $products]);
    
            }catch(Exception $error){
                return $this->fail($error->getMessage());
            }
    
        
    }
    public function saveProduct (Request $request){
        $product = $request->all()['data'];
        $this->check_permissions('products');
        
        //se eliminara ?
        $provider =   $this->business()->products()->where('company_type', 'provider')->where('name', 'Publico en General')->id ?? 0;
        
        $model = $product['id'] == null ? new Product() : $this->business()->products()->find($product['id']);
        
        $model->name = $product['name'];
        $model->provider_id = $provider;
        $model->description = $product['description'];
        $model->price = $product['price'];
        $model->unit = $product['name'];
        $model->purchase_price =$product['purchase_price'];
        $model->type = $product['type'];
        $model->is_inventory = false;
        $model->taxes_amount =$product['taxesAmount'];
        $model->taxes =$product['taxes'];
        $model->is_active =true;
        $this->business()->products()->save($model);

        return $this->success($this->business()->products()->get());
    
    }

    public function deleteProduct ($id){
       
        $this->check_permissions('products_delete');

        $product = $this->business()->products()->find($id);
       
        if(!empty($product)){
            $product->delete();
        }

        
        return $this->success($this->business()->products()->get(), "Producto eliminado correctamente");
    }
}
