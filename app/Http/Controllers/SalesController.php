<?php

namespace App\Http\Controllers;

use App\Http\Requests\billsPostRequest;
use App\Http\Requests\SalesPostRequest;
use App\Managers\SaleManager;
use App\Models\Bill;
use App\Models\BillPayment;
use App\Models\BillsCategory;
use App\Models\BusinessSetting;
use App\Models\Commission;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Sale;
use App\Models\SalePayment;
use App\Models\SalesCategory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spipu\Html2Pdf\Html2Pdf;

class SalesController extends Controller
{
    public $saleManager;
    public function __construct()
    {
        $this->saleManager = new SaleManager();
    }
    public function getTransactionsByType($type, $id){
        $this->check_permissions('sales_access');

        $sales= Sale::sales()->where('clients',$id)->orderByDesc('date')->paginate(10);
        
        $sales = $this->saleManager->formatSales($sales);

        return collect(["transactions" => $sales, "total" => $sales->sum('total')]);
    }

    public function getSales(){

        $this->check_permissions('sales_access');

        $sales = $this->saleManager->formatSales(Sale::sales()->get());
       return view('sales.index', [
            'sales' => $sales->toArray(),
            "total" => $sales->where('status' , '<>' , $this->saleManager::FREE)->sum('total'),
            "clients" => Company::clients()->get(),
            "categories" => $this->business()->salesCategories,
       ]);
    }

    public function getQuotations(){

        $this->check_permissions('sales_access');

        $sales = $this->saleManager->formatSales(Sale::quotations()->get());
       
       return view('quotations.index', [
            'sales' => $sales->toArray(),
            "total" => $sales->where('status' , '<>' , $this->saleManager::FREE)->sum('total'),
            "clients" => Company::clients()->get(),
            "categories" => $this->business()->salesCategories
       ]);
    }

    public function getSalesReportIndex(){
        $this->check_permissions('reports_sales');
       return view('reports.sales');
    }
    

    public function getSale($id = null){

        $this->check_permissions('sales_access');
        $sale = empty($id) ? [] : $this->business()->sales()->where('id', $id)->first() ;
        
        if(!empty($sale)){
                $sale = $this->saleManager->formatSales($sale);
        }
        return view("sales.sales-form", ['sale' => $sale, 
            'categories' => $this->business()->salesCategories()->get(), 
            'clients' => Company::activeClients()->get(),
            'employees' => Employee::actives()->get(),
            'folio' => $this->business()->businessSettings()->where('meta_key', 'last_sales_folio')->first()->meta_value,
            "products" =>$this->business()->products()->get()
 
        ]);
    }

    public function getQuotation($id = null){

        $this->check_permissions('sales_access');
              
        $sale = empty($id) ? [] : Sale::quotations()->where('id', $id)->first();
        
        if(!empty($sale)){
            $sale = $this->saleManager->formatSales($sale);
        }
        ray($sale);
        
        return view("quotations.sales-form", ['sale' => $sale, 
        'categories' => $this->business()->salesCategories()->get(),
        'clients' => Company::clients()->where('active', 1)->get(),
        'employees' => Employee::actives()->get(),
        'folio' => $this->business()->businessSettings()->where('meta_key', 'last_sales_folio')->first()->meta_value,
        "products" =>$this->business()->products()->get()

         ]);
    }


    public function getTotalCommisions($sale){
        
        return $sale->payments->sum('amount') * (($sale->taxes_amount / 100) + 1);
    }

    public function getStatus ($request){
        $manager = new SaleManager();
        $status = $manager::UNPAID;

        if($request->quotation == "true"){
            return $manager::QUOTATION;
        }

        if($request->free == "on"){
            return $manager::FREE;
        }

        if($request->paid == "on"){
            return $manager::PAID;
        }

        return $status;

    }

    public function saveSale(SalesPostRequest $request){
       
        $manager = new SaleManager();
        $permission = $request->id == null ? "sales_create" : "sales_edit";
       
        $this->check_permissions($permission);
       
        $model = $request->id == null ? new Sale() : $this->business()->sales->where('id', $request->id)->first();

        $model->reference = $request->reference;
        $model->date = $request->date;
        $model->description = $request->description;
        $model->categories = $request->categories;
        $model->payment_type = $request->payment_type;
        $model->clients = $request->clients;
        $model->taxes = $request->taxes;
        $model->taxes_amount = $request->taxes_amount;
        $model->status = $this->getStatus($request);
        
        if($request->id == null){
            $model->folio = $this->business()->businessSettings()->where('meta_key', 'last_sales_folio')->first()->meta_value;
            $this->increment(BusinessSetting::where('meta_key', 'last_sales_folio'), 'meta_value');
        }
        $sale = $this->business()->sales()->save($model);
        
        $this->saveSalesPaymentsByRequest($request, $sale);
        $this->saveCommissionsByRequest($request, $sale);

        if($sale->status == $manager::QUOTATION){
            return redirect()->route('update-quotation', ['id' => $sale->id])->with('success', true);    

        }else {
            return redirect()->route('update-sale', ['id' => $sale->id])->with('success', true);    
        }
    }

    public function saveCommissionsByRequest($request, $sale){
        $commissions =  Commission::where('sale_id', $sale->id)->get();

        foreach ($commissions as $key => $commission) {
            $commission->delete();
        }
        ray("nre com", $request->commissions);
        $new_commissions = $request->commissions == null ? [] : json_decode($request->commissions);
        ray("nre com", $new_commissions);
        foreach ($new_commissions as $key => $c) {
            $commission = new Commission();

            $commission->sale_id =  $sale->id;
            $commission->employee_id = $c->employee_id;
            $commission->commission = $c->commission;
            $commission->note = $c->note;
            $commission->amount = $this->getTotalCommisions($sale) *  ($c->commission / 100);
            $this->business()->commissions()->save($commission);

        }
       
    }
    //este se modificara porque no sera ya asi
    public function saveSalesPaymentsByRequest($request, $sale){

        ray(json_decode($request->input('products')));

        $products = json_decode($request->input('products'));
        $products = is_null($products) ? [] :$products;
        SalePayment::where('sale_id', $sale->id)->delete();

        foreach ($products as $product) {
            if(!empty($product->product_id)){
                $payment = new SalePayment();

                $payment->sale_id = $sale->id;
                $payment->taxes = $product->taxes;
                $payment->amount = $product->superTotal;
                $payment->subtotal = $product->superSubTotal;
                $payment->price = $product->price;
                $payment->quantity = $product->quantity;
                $payment->discount = $product->discount;
                $payment->description = $product->description;
                $payment->taxes_amount = $product->taxesAmount;
                $payment->product_id = $product->product_id;
                $payment->format_discount = $product->formatDiscount;
                $this->business()->salesPayments()->save($payment);
            }
            
        }
        return true; 
    }
    public function delete(Request $request){
        $this->check_permissions('sales_delete');
        $id = $request['id'];
        
        $model = $this->business()->sales()->where('id', $id)->first();

        if($model){
            $model->delete();
            return $this->success([],"Eliminado correcamente");
        }

        return $this->fail("Ingreso no encontrado");
       
    }
    public function getSalesByDate(Request $request){
        $reference = $request['reference'];
        $manager =  new SaleManager();
        
        //if the search is with reference
        if(!empty($reference)){

            $sales = Sale::sales()->where(
                function($query) use($reference) {
                  return $query
                         ->where('reference', 'LIKE', '%'. $reference .'%')
                         ->orWhere('folio',  intval(preg_replace("/[^0-9]/", "", $reference )));
                 })->orderByDesc('created_at')->get();         
        }else {

            $sales = Sale::sales();

            //si tiene fechas   

            if(!is_null($request['start']) || !is_null($request['end'])){
                $start = new Carbon($request['start']);
                $end = new Carbon($request['end']);
                $sales->whereBetween('date', [$start->startOfDay(), $end->endOfDay()]);
            }
            //si tiene cliente
            if(!empty(request()->input('client'))){
                $sales->where('clients', request()->input('client'));
            }
            //si tiene categoria
            if(!empty(request()->input('category'))){
                $category = request()->input('category');
                $sales->whereRaw("find_in_set($category,categories)");
            }
            //si tiene estatus
            if(!empty(request()->input('status')) && request()->input('status') !== 'ALL'){
                $sales->where('status', request()->input('status') );
            }
            $sales = $sales->orderByDesc('created_at')->get();
        }
        
       $sales = $manager->formatSales($sales);
    
        $resume = [
            "totalSales" => $sales->where('status', '<>', $manager::FREE)->sum('total'),
            "totalCash"=>$sales->where('status', '<>', $manager::FREE)->where('payment_type', $manager::CASH)->sum('total'),
            "totalCard"=>$sales->where('status', '<>', $manager::FREE)->where('payment_type', $manager::CARD)->sum('total'),
            "totalTaxes"=>$sales->where('status', '<>', $manager::FREE)->sum('taxes_total'),
        ];

        return $this->success([
            "sales" => $sales,
            "total" => $sales->where('status', '<>', $manager::FREE)->sum('total'),
            "resume" =>$resume
        ]);


    }

    public function getQuotationsByDate(Request $request){
        $reference = $request['reference'];
        $manager =  new SaleManager();
        //if the serach is with reference
        if(!empty($reference)){

            $sales = Sale::quotations()->where(
                function($query) use($reference) {
                  return $query
                         ->where('reference', 'LIKE', '%'. $reference .'%')
                         ->orWhere('folio',  intval(preg_replace("/[^0-9]/", "", $reference )));
                 })->orderByDesc('created_at')->get();
            

        }else {

            $sales = Sale::quotations();
       
            //si tiene fechas
            if(!is_null($request['start']) || !is_null($request['end'])){
                $start = new Carbon($request['start']);
                $end = new Carbon($request['end']);
                $sales->whereBetween('date', [$start->startOfDay(), $end->endOfDay()]);
            }

            //si tiene cliente
            if(!empty(request()->input('client'))){
                $sales->where('clients', request()->input('client'));
            }

            //si tiene categoria
            if(!empty(request()->input('category'))){
                $category = request()->input('category');
                $sales->whereRaw("find_in_set($category,categories)");
            }

            //si tiene estatus
            if(request()->input('status') !== 'ALL'){
               
                $sales->where('status', request()->input('status') );
            }

        
            $sales = $sales->orderByDesc('created_at')->get();
        }

       $sales = $manager->formatSales($sales);

        $resume = [
            "totalSales" => $sales->where('status', '<>', $manager::FREE)->sum('total'),
            "totalCash"=>$sales->where('status', '<>', $manager::FREE)->sum('cash'),
            "totalCard"=>$sales->where('status', '<>', $manager::FREE)->sum('card'),
            "totalTaxes"=>$sales->where('status', '<>', $manager::FREE)->sum('taxes_total'),
        ];

        return $this->success([
            "sales" => $sales,
            "total" => $sales->where('status', '<>', $manager::FREE)->sum('total'),
            "resume" =>$resume
        ]);


    }

    public function getSalesClients(Request $request){

        $sales = Sale::sales();
        $start = new Carbon($request['start']);
        $end = new Carbon($request['end']);
        $sales = $sales->whereBetween('date', [$start->startOfDay(), $end->endOfDay()])->get();
        $sales = $this->saleManager->formatSales($sales);
        $clients = Company::clients()->get();
        $model = collect();
        foreach ($clients as $key => $client) {
            $modelTemp = collect();
            $salesCollection =  $sales->filter(function($collection) use($client){ 
                return in_array($client->id, explode(',',$collection->clients)); 
              });
            $modelTemp['client'] = $client->name;
            $modelTemp['sales'] = count($salesCollection->where('free', 0));
            $modelTemp['total'] = $salesCollection->sum('total');
            $modelTemp['free'] = count($salesCollection->where('status', $this->saleManager::FREE));
            $modelTemp['lastSale'] = empty($salesCollection->sortByDesc('date')->first()) ? '' : $salesCollection->sortByDesc('date')->first()->date->format('d-m-Y');
            $model->push($modelTemp);
        }

        return $this->success([
            "sales" => $model,
            "resume" =>[]
        ]);


    }

    public function getSalesCategories(Request $request){

        $sales = Sale::sales();
        $start = new Carbon($request['start']);
        $end = new Carbon($request['end']);
        $sales = $sales->whereBetween('date', [$start->startOfDay(), $end->endOfDay()])->get();
        $sales = $this->saleManager->formatSales($sales);

        $categories = $this->business()->salesCategories;
        $model = collect();
        foreach ($categories as $category) {
            $modelTemp = collect();
            $salesCollection =  $sales->filter(function($collection) use($category){ 
                return in_array($category->id, explode(',',$collection->categories),); 
              });
            $modelTemp['category'] = $category->name;
            $modelTemp['sales'] = count($salesCollection->where('free', 0));
            $modelTemp['total'] = $salesCollection->sum('total');
            $modelTemp['free'] = count($salesCollection->where('status', $this->saleManager::FREE));
            $modelTemp['lastSale'] = empty($salesCollection->sortByDesc('date')->first()) ? '' : $salesCollection->sortByDesc('date')->first()->date->format('d-m-Y');
            $model->push($modelTemp);
        }
        return $this->success([
            "sales" => $model,
            "resume" =>[]
        ]);


    }

    public function getReportsSales(Request $request){

        $type = $request['type'];
        if($type == "general"){
            return $this->getSalesByDate($request);
        }
        if($type == "top-clients"){
            return $this->getSalesClients($request);
        }
        if($type == "top-categories"){
            return $this->getSalesCategories($request);
        }

    }

    public function viewSalePDF($id){
        $sale = $this->business()->sales()->find($id);
        $html2pdf = new Html2Pdf();
        $html = view('sales.sale-pdf', ['sale' => $sale]);
        $html2pdf->writeHTML($html);
        return $html2pdf->output();
    }
}
