<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompaniesPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:100',
            'rfc' => 'nullable|min:4|max:15',
            'email' => 'nullable|email|max:50',
            'phone' => 'nullable|max:10',
            'address' => 'nullable|min:2|max:50',
            'city' => 'nullable|min:2|max:50',
            'zipcode' => 'nullable|min:2|max:10',
            'state_id' => 'nullable|max:6',
            'notes' => 'nullable|max:1000',
            'legal_name' => 'nullable',

        ];



        // $addRules = [
        //     'legal_type' => 'required',
        // ];

        // if( !empty(request()->input('rfc')) ){
        //     $rules = array_merge($rules, $addRules);
        // }

        return $rules;
    }
}
