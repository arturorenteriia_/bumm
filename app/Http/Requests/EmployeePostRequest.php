<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $rules = [
            'name' => 'required|min:2|max:100',
            'last_name' => 'required|min:2|max:100',
            'email' => 'nullable|email|max:50',
            'phone' => 'nullable|max:10',
            'address' => 'nullable|min:2|max:50',
            'city' => 'nullable|min:2|max:50',
            'zipcode' => 'nullable|min:2|max:10',
            'state_id' => 'nullable|max:6',
            'birth' => 'nullable|date_format:d-m-Y',
            'payment_type' => 'required',
            'account' => 'nullable',
            'commission' => 'required_if:paymentType,commission,mixed|numeric',
            'salary' => 'required_if:paymentType,salary,mixed|numeric',
            'username' => 'required_if:account,on|email',

        ];

        $passRules = [
            'password' => 'required|min:6|max:15',
            'password_confirm' => 'same:password',
        ];

        if(
        (!is_null(request()->input('password')) && request()->input('account') == "on") ||
        (request()->input('id') == null &&  request()->input('account') == "on")
        ){
            $rules = array_merge($rules, $passRules);
        }

        return $rules;
    }
}
