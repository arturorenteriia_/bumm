<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class billsPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date' => 'required|date_format:d-m-Y',
            'reference' => 'nullable|min:1|max:50',
            'categories' => 'required',
            'providers' => 'required',
            'description' => 'nullable|max:500',
            'cash' => 'required_without:card',
            'card' => 'required_without:cash',
            'paid' => 'nullable',

        ];



        // $addRules = [
        //     'legal_type' => 'required',
        // ];

        // if( !empty(request()->input('rfc')) ){
        //     $rules = array_merge($rules, $addRules);
        // }

        return $rules;
    }
}
