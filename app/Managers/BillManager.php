<?php
namespace App\Managers;

use App\Http\Controllers\Controller;
use App\Models\BillPayment;
use App\Models\BillsCategory;
use App\Models\Company;

class BillManager extends Controller{
    
    const PAID = "PAID";
    const UNPAID = "UNPAID";
    const QUOTATION = "QUOTATION";
    const FREE = "FREE";

    public   function formatBills ($bills){
        if(is_countable($bills)){
            foreach ($bills as  $bill) {
                $bill = $this->addProperties($bill);
            }
        } else{
            $bills = $this->addProperties($bills);
        }

        return $bills;
    }

    public function addProperties ($bill) {
        $bill['total'] = $bill->payments->sum('amount');
        $bill['categories_names'] = $this->getCategories($bill->categories);
        $bill['providers_names'] = $this->getProviders($bill->providers);
        $bill['cash'] = strval($bill->payments()->where('payment_type', 'cash')->first()->amount);
        $bill['card'] = strval($bill->payments()->where('payment_type', 'card')->first()->amount);
        $bill['paid_text'] = $bill->paid ? "Si" : "No";
        $bill['free_text'] = $bill->free ? "Si" : "No";
        return $bill;
}

    public function getCategories($ids){
        $idsArray = explode(",", $ids);
        
        $model = $this->business()->billsCategories()->whereIn('id', $idsArray)->get();
        $names = array();
        foreach ($model as $m) {
        array_push($names, $m->name);
        }

        return implode(", ", $names);
    }

    public function getProviders($ids){
        $idsArray = explode(",", $ids);
        
        $model = Company::providers()->whereIn('id', $idsArray)->get();
        $names = array();
        foreach ($model as $m) {
        array_push($names, $m->name);
        }

        return implode(", ", $names);
    }

}