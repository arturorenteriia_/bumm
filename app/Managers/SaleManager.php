<?php
namespace App\Managers;

use App\Http\Controllers\Controller;
use App\Models\Commission;
use App\Models\Sale;
use App\Models\SalesCategory;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;

class SaleManager extends Controller{
    
    const PAID = "PAID";
    const UNPAID = "UNPAID";
    const QUOTATION = "QUOTATION";
    const FREE = "FREE";
    const CASH = "cash";
    const TRANSFER = "transfer";
    const CARD = "card";


    public   function formatSales ($sales){
        if(is_countable($sales)){
            foreach ($sales as  $sale) {
                $sale = $this->addSalesProperties($sale);
            }
        } else{
            $sales = $this->addSalesProperties($sales);
        }

        return $sales;
    }
    public  function getNamesByID($ids, $model){
        $idsArray = explode(",", $ids);
        $app = "\App\Models".$model;
        $categories = $app::whereIn('id', $idsArray)->where('company_id', Auth::user()->company_id)->get();
        $categoriesNames = array();
        foreach ($categories as $key => $category) {
           array_push($categoriesNames, $category->name);
        }

        return implode(", ", $categoriesNames);
    }

    public function addSalesProperties ($sale) {

            $sale['total'] = $sale->payments->sum('amount');
            $sale['categories_names'] = $sale->category->name;
            $sale['taxes_total'] = $sale->payments->sum('taxes_amount');
            $sale['discount_total'] = $sale->payments->sum('discount');
            $sale['clients_names'] = $sale->client->name;
            $sale['total_commissions'] = $sale->commissions->sum('amount');
            $sale['commissions_name'] = $this->getCommissions($sale->id);
            $sale['employees_name'] = $this->getCommisionsAgentsName($sale->commission_agents);

            return $sale;
    }

    public  function getPaymentsTotal($sale){
        return $sale->payments->sum('amount') * (($sale->taxes_amount / 100) + 1);
    }

    public function getCommisionsAgentsName($agents){

        $names = "";    
        foreach ($agents as $key => $agent) {
           $names = $names.$agent->employee->name. ' ' . $agent->employee->last_name;
           if(count($agents) > 1 && $key < count($agents) - 1 ){
            $names =  $names.', ';
           }
        }
        return $names;
    }
    public function getAmount($sale, $type){
        $amount = $sale->payments->where('payment_type', $type)->first();
        return empty($amount) ? "0" : strval($amount->amount);
    }

    public function getCommissions($saleID){
         $model = $this->business()->commissions()->where('sale_id', $saleID)->get();
        
         foreach ($model as $key => $c) {
            if(!empty($c->employee)){
                $c['name'] = $c->employee->name . ' ' .$c->employee->last_name;
            }else{
                $c['name'] = 'Nombre no disponible';
            }
        }

        return $model;
    } 

    

}