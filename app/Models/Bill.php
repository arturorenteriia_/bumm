<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bill extends Model
{
    use HasFactory;
    protected $casts = [
        'created_at' => 'date:d-m-Y - h:i:s A',
        'date' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y - h:i:s A',
    ];
    public function payments()
    {
        return $this->hasMany(BillPayment::class);
    }

    public function created_user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function updated_user()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
    public function setDateAttribute( $value ) {
        $this->attributes['date'] = (new Carbon($value))->format('Y/m/d');
    }
    public static function boot() {
        parent::boot();
        static::creating(function($bill) { // before delete() method call this
            $bill->created_by = Auth::user()->id;

        });
        static::deleting(function($model) { // before delete() method call this
             $model->payments()->delete();
             
        });
        static::updating(function($model) { // before update() method call this
            $model->updated_by = Auth::user()->id;
        });
        
    }
}
