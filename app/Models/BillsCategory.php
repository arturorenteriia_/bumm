<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BillsCategory extends Model
{
    protected $table = 'bills_categories';
    use HasFactory;

    public static function boot() {
        parent::boot();
        static::creating(function($c) { // before delete() method call this
            $c->created_by = Auth::user()->id;
        });

    }
}
