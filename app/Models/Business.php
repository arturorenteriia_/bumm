<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [
        'name',
        'email',
        'type',
    ];
    use HasFactory;

    public function sales (){
        return $this->hasMany(Sale::class, 'company_id', 'id');
    }

    public function bills (){
        return $this->hasMany(Bill::class, 'company_id', 'id');
    }

    public function users (){
        return $this->hasMany(User::class, 'company_id', 'id');
    }

    public function employees (){
        return $this->hasMany(Employee::class, 'company_id', 'id');
    }

    public function companies (){
        return $this->hasMany(Company::class, 'company_id', 'id');
    }

    public function salesCategories (){
        return $this->hasMany(SalesCategory::class, 'company_id', 'id');

    }

    public function products (){
        return $this->hasMany(Product::class, 'company_id', 'id');

    }

    public function billsCategories (){
        return $this->hasMany(BillsCategory::class, 'company_id', 'id');

    }

    public function commissions (){
        return $this->hasMany(Commission::class, 'company_id', 'id');

    }

    public function businessSettings (){
        return $this->hasMany(BusinessSetting::class, 'company_id', 'id');
    }

    public function salesPayments (){
        return $this->hasMany(SalePayment::class, 'company_id', 'id');
    }
}
