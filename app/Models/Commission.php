<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Commission extends Model
{
    use HasFactory;

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }
    public function sale()
    {
        return $this->hasOne(Sale::class, 'id', 'sale_id')->with(['payments']);
    }

    public function scopeSales($model)
    {
        return  $model->where('company_id', Auth::user()->company_id) ;
    }

    public static function boot() {
        parent::boot();
        static::creating(function($sale) { // before delete() method call this
            $sale->created_by = Auth::user()->id;
        });

        static::updating(function($model) { // before update() method call this
            $model->updated_by = Auth::user()->id;
        });

    }
}
