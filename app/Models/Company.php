<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model
{
    use HasFactory;

    public function scopeClients ($model){
        return $model->where('company_type', 'client')->where('company_id', Auth::user()->company_id)->orderByDesc('created_at');
    }
    public function scopeProviders ($model){
        return $model->where('company_type', 'provider')->where('company_id', Auth::user()->company_id)->orderByDesc('created_at');
    }

    public function scopeActiveClients ($model){
        return $model->where('company_type', 'client')->where('company_id', Auth::user()->company_id)->where('active', 1)->orderByDesc('created_at');
    }

    public static function boot() {
        parent::boot();
        static::creating(function($company) { // before delete() method call this
            $company->created_by = Auth::user()->id;

        });
        static::updating(function($model) { // before update() method call this
            $model->updated_by = Auth::user()->id;
        });
    }
}
