<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Employee extends Model
{
    use HasFactory;

    protected $casts = [
        'created_at' => 'date:d-m-Y - h:i:s A',
        'birth' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y - h:i:s A',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function commissions()
    {
        return $this->hasMany(Commission::class);
    }

    public function scopeActives($query)
    {
        return $query->where('active', true)->where('company_id', Auth::user()->company_id);
    }

    public function permissions()
    {
        return $this->hasOne(User::class);
    }
    public function setBirthAttribute( $value ) {
        $this->attributes['birth'] = (new Carbon($value))->format('Y/m/d');
    }
    public static function boot() {
        parent::boot();
        static::creating(function($model) { // before delete() method call this
            $model->created_by = Auth::user()->id;

        });
        static::deleting(function($model) { // before delete() method call this
            $model->commissions()->delete();
       });
       static::updating(function($model) { // before update() method call this
        $model->updated_by = Auth::user()->id;
    });

    }
}
