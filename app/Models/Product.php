<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function scopeProducts ($query){
        return $query->where('is_active', true)->where('company_id', Auth::user()->company_id);
    }

    public static function boot() {
        parent::boot();
        static::creating(function($model) { // before delete() method call this
            $model->created_by = Auth::user()->id;

        });
        static::updating(function($model) { // before delete() method call this
             $model->updated_by = Auth::user()->id;
        });

    }
}
