<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;

class Sale extends Model
{
    use HasFactory;
    protected $casts = [
        'created_at' => 'date:d-m-Y - h:i:s A',
        'date' => 'date:d-m-Y',
        'updated_at' => 'date:d-m-Y - h:i:s A',
    ];
    public function payments()
    {
        return $this->hasMany(SalePayment::class);
    }

    public function client()
    {
        return $this->hasOne(Company::class, 'id', 'clients');
    }

    public function category()
    {
        return $this->hasOne(SalesCategory::class, 'id', 'categories');
    }

    public function commissions()
    {
        return $this->hasMany(Commission::class);
    }

    public function commission_agents()
    {
        return $this->hasMany(Commission::class)->with('employee');
    }

    public function created_user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function updated_user()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
    public function setDateAttribute( $value ) {
        $this->attributes['date'] = (new Carbon($value))->format('Y/m/d');
    }

    public function scopeSales($model){
        return  $model->where('company_id', Auth::user()->company_id)->where('status' ,'<>', 'QUOTATION') ;
    }

    public function scopeQuotations($model){
        return  $model->where('company_id', Auth::user()->company_id)->where('status' , 'QUOTATION') ;
    }

    public static function boot() {
        parent::boot();
        static::creating(function($sale) { // before delete() method call this
            $sale->created_by = Auth::user()->id;

        });
        static::deleting(function($sale) { // before delete() method call this
             $sale->payments()->delete();
             $sale->commissions()->delete();
        });

        static::updating(function($model) { // before update() method call this
            $model->updated_by = Auth::user()->id;
        });

    }
}
