<?php

use App\Models\BusinessSetting;
use Illuminate\Support\Arr;

if (! function_exists('current_user')) {
    function has_permission($permission)
    {
        if(auth()->user()->is_admin || auth()->user()->super_user){
            return true;
        }
        $permissions = explode(",",auth()->user()->permissions);

        if(gettype($permission) == "array"){
            foreach ($permission as $p) {
                if(in_array($p, $permissions)){
                    return true;
                }
            }
        }

       return in_array($permission, $permissions);
    }


}

