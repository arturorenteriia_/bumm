<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            // $table->id();
            // $table->foreignId('client_id')->nullable();
            // $table->foreignId('company_id');
            // $table->string('invoice_number')->nullable();
            // $table->date('date');
            // $table->float('simple_amount')->nullable();
            // $table->float('taxes')->nullable();
            // $table->float('total_amount');
            // $table->float('amount_without_commissions')->nullable();
            // $table->string('created_by');
            // $table->string('charged')->nullable();
            // $table->date('charged_date')->nullable();
            // $table->string('description')->nullable();
            // $table->string('updated_by')->nullable();
            // $table->boolean('has_taxes')->nullable();
            // $table->string('taxes_percent')->nullable();
            // $table->timestamps();
            $table->id();
            $table->string('description')->nullable();
            $table->string('reference')->nullable();
            $table->string('categories');
            $table->string('clients');
            $table->string('taxes')->nullable();
            $table->string('taxes_amount')->nullable();
            $table->string('discount')->nullable();
            $table->boolean('paid');
            $table->boolean('free');
            $table->date('date');
            $table->foreignId('company_id');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
};
