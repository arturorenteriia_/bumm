<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.x
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->string('reference')->nullable();
            $table->string('categories');
            $table->string('providers');
            $table->boolean('paid');
            $table->date('date');
            $table->string('taxes')->nullable();
            $table->string('taxes_amount')->nullable();
            $table->foreignId('company_id');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_bills');
    }
};
