<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id');
            $table->foreignId('provider_id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->float('price')->nullable();
            $table->string('unit')->nullable();
            $table->float('purchase_price')->nullable();
            $table->string('type');
            $table->boolean('is_inventory');
            $table->float('taxes_amount');
            $table->string('taxes');
            $table->string('updated_by')->nullable();
            $table->string('created_by');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
