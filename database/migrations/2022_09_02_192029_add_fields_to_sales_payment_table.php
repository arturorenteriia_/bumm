<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_payments', function (Blueprint $table) {
            $table->float('price');
            $table->float('quantity');
            $table->foreignId('product_id');
            $table->float('discount')->nullable();
            $table->float('subtotal');
            $table->float('taxes_amount')->nullable();
            $table->longText('description')->nullable();
            $table->longText('format_discount')->nullable();
            $table->dropColumn('payment_type');
        });

        Schema::table('sales', function (Blueprint $table) {
            $table->string('payment_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_payment', function (Blueprint $table) {
            //
        });
    }
};
