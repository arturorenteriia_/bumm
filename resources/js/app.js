require('./bootstrap');

import Alpine from 'alpinejs';
window.Alpine = Alpine;

Alpine.start();

import { createApp } from 'vue';
import PrimeVue from 'primevue/config';

//PRIMEVUE framework
import 'primevue/resources/themes/saga-blue/theme.css'       //theme
import 'primevue/resources/primevue.min.css'                 //core css
import 'primeicons/primeicons.css' 

import useBreakpoints from "vue-next-breakpoints";


//BUMM components
import SalesIndex from '../js/components/sales/Index.vue'
import QuotationsIndex from '../js/components/quotations/Index.vue'

import Products from '../js/components/sales/Products.vue'

import BillIndex from '../js/components/bills/Index.vue'
import ClientsIndex from '../js/components/clients/Index.vue'
import SalesForm from '../js/components/sales/Form.vue'
import QuotationsForm from '../js/components/quotations/Form.vue'

import BillsForm from '../js/components/bills/Form.vue'
import Categories from '../js/components/config/Categories.vue'
import CompanyProfile from '../js/components/config/CompanyProfile.vue'

import BillsCategories from '../js/components/config/BillsCategories.vue'
import ConfigEmployeesIndex from '../js/components/config/Employees.vue'
import EmployeeForm from '../js/components/config/EmployeeForm.vue'
import CardTitle from '../js/components/reusable/cardTitle.vue'
import CompanyForm from '../js/components/clients/CompanyForm.vue'
import SalesReport from '../js/components/reports/sales.vue'
import BillsReport from '../js/components/reports/bills.vue'
import CommissionsReport from '../js/components/reports/commissions.vue'
import IncomesReport from '../js/components/reports/incomes.vue'

//PRIMEVUE components
import InputText from 'primevue/inputtext';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import Card from 'primevue/card';
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';
import Chart from 'primevue/chart';
import Button from 'primevue/button';
import Calendar from 'primevue/calendar';
import Textarea from 'primevue/textarea';
import Sidebar from 'primevue/sidebar';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
import BlockUI from 'primevue/blockui';
import ConfirmPopup from 'primevue/confirmpopup';
import ConfirmationService from 'primevue/confirmationservice';
import Toolbar from 'primevue/toolbar';
import Tooltip from 'primevue/tooltip';
import InputSwitch from 'primevue/inputswitch';
import RadioButton from 'primevue/radiobutton';
import Message from 'primevue/message';
import Chip from 'primevue/chip';
import ConfirmDialog from 'primevue/confirmdialog';
import Dropdown from 'primevue/dropdown';
import InputNumber from 'primevue/inputnumber';
import MultiSelect from 'primevue/multiselect';
import Checkbox from 'primevue/checkbox';
import Badge from 'primevue/badge';
import Paginator from 'primevue/paginator';
import SplitButton from 'primevue/splitbutton';
import TieredMenu from 'primevue/tieredmenu';
import FileUpload from 'primevue/fileupload';
import Image from 'primevue/image';

//VUE instance
const app = createApp({
    data() {
        return {
          salesStatuses:{
            "PAID":"Pagado",
            "UNPAID":"No pagado",
            "QUOTATION":"Cotización",
            "FREE":"Cortesia",
          }
          ,
          taxes:[
            {
             name: 'Ninguno 0%',
             code: 'none',
             tax:0
           },
           {
             name: 'IVA 16%',
             code: 'IVA-16',
             tax:16
           },
            {
             name: 'IVA 8%',
             code: 'IVA-8',
             tax:8
           },
            {
             name: 'IVA 0%',
             code: 'IVA-0',
             tax:0
           },
            {
             name: 'IEPS 8%',
             code: 'IEPS-8',
             tax:8
           },
            {
             name: 'IEPS 0%',
             code: 'IEPS-0',
             tax:0
           }
         ],
          salesItemsMenu:[
          {
              label: 'Ingresos',
              icon: 'pi pi-money-bill',
              command: () => {
                window.location.href = '/sales'
              }
          },
          {
            label: 'Cotizaciones',
            icon: 'pi pi-clone',
            command: () => {
              window.location.href = '/quotations'
            }
        },
          {
              label: 'Productos y servicios',
              icon: 'pi pi-shopping-cart',
              command: () => {
                window.location.href = '/sales/products'
              }
          },
          ],
          register:{},
          login:{},
          breakpoint:  useBreakpoints({
            mobile: 600, // max-width: 600px
            tablet: 992,
            desktop: [993]
        }),
          css:{
            openMenu : true,
            fullMainContainer: false,
          },
          isLoadingScreen: false,
          singleCategory:null,
          months: [
            {label: 'Visualizar el año completo', value: 0},
            {label: 'Enero', value: 1},
            {label: 'Febrero', value: 2},
            {label: 'Marzo', value: 3},
            {label: 'Abril', value: 4},
            {label: 'Mayo', value: 5},
            {label: 'Junio', value: 6},
            {label: 'Julio', value: 7},
            {label: 'Agosto', value: 8},
            {label: 'Septiembre', value: 9},
            {label: 'Octubre', value: 10},
            {label: 'Noviembre', value: 11},
            {label: 'Diciembre', value: 12}
          ],
          states:[
            { "code": "AGS", "name": "Aguascalientes" },
            { "code": "BC",  "name": "Baja california" },
            { "code": "BCS", "name": "Baja california sur" },
            { "code": "CHI", "name": "Chihuahua" },
            { "code": "CHS", "name": "Chiapas" },
            { "code": "CMP", "name": "Campeche" },
            { "code": "CMX", "name": "Ciudad de mexico" },
            { "code": "COA", "name": "Coahuila" },
            { "code": "COL", "name": "Colima" },
            { "code": "DGO", "name": "Durango" },
            { "code": "GRO", "name": "Guerrero" },
            { "code": "GTO", "name": "Guanajuato" },
            { "code": "HGO", "name": "Hidalgo" },
            { "code": "JAL", "name": "Jalisco" },
            { "code": "MCH", "name": "Michoacan" },
            { "code": "MEX", "name": "Estado de mexico" },
            { "code": "MOR", "name": "Morelos" },
            { "code": "NAY", "name": "Nayarit" },
            { "code": "NL",  "name": "Nuevo leon" },
            { "code": "OAX", "name": "Oaxaca" },
            { "code": "PUE", "name": "Puebla" },
            { "code": "QR",  "name": "Quintana roo" },
            { "code": "QRO", "name": "Queretaro" },
            { "code": "SIN", "name": "Sinaloa" },
            { "code": "SLP", "name": "San luis potosi" },
            { "code": "SON", "name": "Sonora" },
            { "code": "TAB", "name": "Tabasco" },
            { "code": "TLX", "name": "Tlaxcala" },
            { "code": "TMS", "name": "Tamaulipas" },
            { "code": "VER", "name": "Veracruz" },
            { "code": "YUC", "name": "Yucatan" },
            { "code": "ZAC", "name": "Zacatecas" } 
        ]
        }
      },
      computed:{
        years(){
          let years = [];
          let currentYear = new Date();
          currentYear = currentYear.getFullYear() + 5;
          for (let index = 2020; index < currentYear; index++) {
            years.push({
              "value": index,
              "label": index
            });
          }

          return years;
        }
      },
      methods:{
        toggle(event){
          console.log(this.$refs.menu);
          this.$refs.menu.toggle(event);
        },
        fullMenu(){
          this.css.openMenu = !this.css.openMenu;
          if(!this.breakpoint.mobile.matches){
            this.css.fullMainContainer = !this.css.fullMainContainer;
          }

        },
        openUrl(url, target){
          window.open(url, target ?? "_self");
        },
        alert(severity, title, message){
          this.$toast.add({severity: severity, summary: title, detail: message, life: 3000});

        },
        loading(state){
          this.isLoadingScreen = state;
        },
        currencyFormat(amount){
            amount = amount ? amount : 0;
            return amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
      },
      mounted(){
        this.css.fullMainContainer = this.breakpoint.mobile.matches;
        this.css.openMenu = this.breakpoint.desktop.matches;        
        this.breakpoint.mobile.on("enter", (mq) => {
          this.css.fullMainContainer = true;
          this.css.openMenu = false;
      });

      this.breakpoint.tablet.on("enter", (mq) => {
        this.css.fullMainContainer = true;
        this.css.openMenu = false;
      });
      this.breakpoint.desktop.on("enter", (mq) => {
        this.css.fullMainContainer = false;
        this.css.openMenu = true;

      });

      }
  });


//VUE components
app.component('SaleIndex', SalesIndex);
app.component('QuotationsIndex', QuotationsIndex);

app.component('SalesForm', SalesForm);
app.component('BillsForm', BillsForm);
app.component('QuotationsForm', QuotationsForm);

app.component('categories', Categories);
app.component('products', Products);

app.component('BillsCategories', BillsCategories);
app.component('EmployeeForm', EmployeeForm);
app.component('CardTitle', CardTitle);
app.component('CompanyForm', CompanyForm);
app.component('BillIndex', BillIndex);
app.component('CompaniesIndex', ClientsIndex);
app.component('ConfigEmployeesIndex', ConfigEmployeesIndex);
app.component('SalesReport', SalesReport);
app.component('BillsReport', BillsReport);
app.component('CommissionsReport', CommissionsReport);
app.component('IncomesReport', IncomesReport);
app.component('CompanyProfile', CompanyProfile)

//PRIMEVUE components
app.component('PInput', InputText);
app.component('DataTable', DataTable);
app.component('column', Column);
app.component('accordion', Accordion);
app.component('AccordionTab', AccordionTab);
app.component('card', Card);
app.component('PChart', Chart);
app.component('PButton', Button);
app.component('PCalendar', Calendar);
app.component('PTextarea', Textarea);
app.component('PSidebar', Sidebar);
app.component('PAlert', Toast);
app.component('PBlock', BlockUI);
app.component('PConfirm', ConfirmPopup);
app.component('PToolbar', Toolbar);
app.component('PSwitch', InputSwitch);
app.component('PRadio', RadioButton);
app.component('PMessage', Message);
app.component('PChip', Chip);
app.component('PDialog', ConfirmDialog);
app.component('p-dropdown', Dropdown);
app.component('PInputnumber', InputNumber);
app.component('PMultiselect',MultiSelect);
app.component('PCheckbox',Checkbox);
app.component('PBadge',Badge);
app.component('PPaginator',Paginator);
app.component('PSplitbutton',SplitButton);
app.component('PTieredmenu',TieredMenu);
app.component('PFileupload',FileUpload);
app.component('PImage',Image);

app.directive('tooltip', Tooltip);
app.use(ToastService);
app.use(ConfirmationService);

app.use(PrimeVue, {
  locale: {
    startsWith: 'Starts with',
    contains: 'Contains',
    notContains: 'Not contains',
    endsWith: 'Ends with',
    equals: 'Equals',
    notEquals: 'Not equals',
    noFilter: 'No Filter',
    lt: 'Less than',
    lte: 'Less than or equal to',
    gt: 'Greater than',
    gte: 'Greater than or equal to',
    dateIs: 'Date is',
    dateIsNot: 'Date is not',
    dateBefore: 'Date is before',
    dateAfter: 'Date is after',
    clear: 'Limpiar',
    apply: 'Apply',
    matchAll: 'Match All',
    matchAny: 'Match Any',
    addRule: 'Add Rule',
    removeRule: 'Remove Rule',
    accept: 'Si',
    reject: 'No',
    choose: 'Choose',
    upload: 'Upload',
    cancel: 'Cancel',
    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
    monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
    monthNamesShort: ["Ene", "Feb", "Mar", "Abri", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: 'Hoy',
    weekHeader: 'Wk',
    firstDayOfWeek: 1,
    dateFormat: 'dd-mm-yy',
    weak: 'Weak',
    medium: 'Medium',
    strong: 'Strong',
    passwordPrompt: 'Enter a password',
    emptyFilterMessage: 'No results found',
    emptyMessage: 'No available options'
}
});

app.mount("#app");