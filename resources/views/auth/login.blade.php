<x-guest-layout>
    {{-- <x-auth-card>
        <x-slot name="logo">
            <a href="/">
               
            </a>
        </x-slot>

       

        <form method="POST" action="/logina">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your passasdsword?') }}
                    </a>
                @endif

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card> --}}

    <div class="container">
        <div id="app"> 
            <div class="row login-box">
             <div class="col-sm-9 col-md-4">
                 <card>
                     <template #content>
                         <div><h3>Iniciar sesion</h3></div>
                         <form method="POST" action="/logina">
                             @csrf
                         <div class="form-properties">
                             <div class="row">
                                 <div class="col-sm-12">
                                     <span class="p-float-label">
                                         <p-input name="email" v-model="login.email"  id="email" type="email"  ></p-input>
                                         <label for="reference">Email</label>
                                     </span>
                                 </div>
                                 <div class="col-sm-12">
                                     <span class="p-float-label">
                                         <p-input v-model="login.password" name="password"  id="password" type="password" ></p-input>
                                         <label for="reference">Contraseña</label>
                                     </span>
                                 </div>
                                 <div class="col-sm-12 text-right">
                                     <p-button type="submit" class="mt-3 p-button-primary" label="Entrar"  ></p-button>
     
                                 </div>
                                 <div class="col-sm-12 mt-4">
                                     <a class="link" href="/register">Registrar nueva empresa</a>
                                 </div>
                             </div>
                            
                             
                             {{-- <span class="p-float-label" style="margin-top: 0px">
                                 <p-checkbox 
                                 class="mt-3" 
                                 name="remember_me"
                                 value="remember_me" id="remember_me"  
                                 >
                                 </p-checkbox>
                                 <label style="margin-left: 1.2rem" 
                                     for="account">Recuerdame
                                 </label>
                             </span> --}}
                         </div>
                     </form>
                     <div class="row">
                         <div class="col-sm-12 mt-3">
                              <!-- Session Status -->
                             <x-auth-session-status class="mb-4" :status="session('status')" />
     
                             <!-- Validation Errors -->
                             <x-auth-validation-errors class="mb-4" :errors="$errors" />
                         </div>
                     </div>
                     </template>
                 </card>
                </div>
            </div>
         </div>  
    </div>
</x-guest-layout>
