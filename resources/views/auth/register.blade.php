<x-guest-layout>
        {{-- <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form> --}}

        <div class="container">
            <div id="app"> 
                <div class="row login-box">
                 <div class="col-sm-9 col-md-4">
                     <card>
                         <template #content>
                             <div><h3>Crear nueva empresa</h3></div>
                             <form method="POST" action="{{ route('register') }}">
                                 @csrf
                             <div class="form-properties">
                                 <div class="row">
                                    <div class="col-sm-12">
                                        <span class="p-float-label">
                                            <p-input name="company_name"  id="company_name" type="text" v-model="register.company_name"  ></p-input>
                                            <label for="company_name">Nombre de la empresa</label>
                                        </span>
                                    </div>
                                    <div class="col-sm-12">
                                        <span class="p-float-label">
                                            <p-input name="name"  id="name" type="text"  v-model="register.name"></p-input>
                                            <label for="name">Nombre del representante legal</label>
                                        </span>
                                    </div>
                                     <div class="col-sm-12">
                                         <span class="p-float-label">
                                             <p-input name="email"  id="email" type="email"  v-model="register.email"></p-input>
                                             <label for="reference">Email</label>
                                         </span>
                                     </div>
                                     <div class="col-sm-12">
                                         <span class="p-float-label">
                                             <p-input name="password"  id="password" type="password" v-model="register.password"></p-input>
                                             <label for="reference">Contraseña</label>
                                         </span>
                                     </div>
                                     <div class="col-sm-12">
                                        <span class="p-float-label">
                                            <p-input name="password_confirmation"  id="password_confirmation" type="password" v-model="register.password2"></p-input>
                                            <label for="password_confirmation">Repetir contraseña</label>
                                        </span>
                                    </div>
                                     <div class="col-sm-12 text-right">
                                         <p-button type="submit" class="mt-3 p-button-primary" label="Entrar"  ></p-button>
         
                                     </div>
                                     <div class="col-sm-12 mt-4">
                                         <a class="link" href="/login">Regresar a login</a>
                                     </div>
                                 </div>
                                
                                 
                                 {{-- <span class="p-float-label" style="margin-top: 0px">
                                     <p-checkbox 
                                     class="mt-3" 
                                     name="remember_me"
                                     value="remember_me" id="remember_me"  
                                     >
                                     </p-checkbox>
                                     <label style="margin-left: 1.2rem" 
                                         for="account">Recuerdame
                                     </label>
                                 </span> --}}
                             </div>
                         </form>
                         <div class="row">
                             <div class="col-sm-12 mt-3">
                                  <!-- Session Status -->
                                 <x-auth-session-status class="mb-4" :status="session('status')" />
         
                                 <!-- Validation Errors -->
                                 <x-auth-validation-errors class="mb-4" :errors="$errors" />
                             </div>
                         </div>
                         </template>
                     </card>
                    </div>
                </div>
             </div>  
        </div>
</x-guest-layout>
