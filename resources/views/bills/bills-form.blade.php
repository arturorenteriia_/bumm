<x-app-layout>
        <div class="full-w">
            <div class="p-breadcrumb p-component">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/bills" class="p-menuitem-link">
                        <span class="p-menuitem-text">Egresos</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Agregar / Editar</span>
                    </li>
                
                </ul>
            </div>
           </div>
    
   <bills-form folio="{{$folio}}" :providers="{{$providers}}" :categories="{{json_encode($categories)}}"   v-slot="f" bill="{{count(request()->old()) > 0 ? json_encode(request()->old()) : (Route::currentRouteName() == "update-bill" ? json_encode($bill) : json_encode([]))}}">
    
    <div v-if="f.form.id" class="row mb-3">
        <div class="col-sm-6 mt-3 text-center ">
            <card-title title="Detalle de egreso"  :filter="true" color="white"></card-title>
        </div>
        <div class="col-sm-6 mt-3 ">
            <card class="no-shadow br-20">
                <template #content>
                    @if(has_permission('bills_create'))
                        <div class="row">
                            <div class="col-sm-12 d-flex">
                                <div style="display: grid">
                                    <p-button @click="openUrl('/bills/bill')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                    <small class="mt-2">Agregar</small>
                                </div>
        
                                <div style="display: grid" class="ml-3">
                                    <p-button @click="openUrl('/bills/pdf/'+f.form.id, '_blank')"  class="big-icon-button export-pdf-button" icon="pi pi-file-pdf"></p-button>
                                    <small class="mt-2">Ver PDF</small>
                                </div>
                            </div>
                        </div>
                    @endif
                </template>
            </card>
        </div>
    </div>
    <card-title class="mt-3 mb-3 " v-else title="Agregar nuevo egresos"  :filter="true" color="white"></card-title>

    
    <card class="no-shadow">
        <template #content>
            <form action="/bills/save-bill" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <p-input  name="id" id="id" type="hidden" v-model="f.form.id" ></p-input>
                <p-input  name="company_type" id="company_type" type="hidden" model-value="provider" ></p-input>
            <div class="row form-properties " style="position: relative;">
            
                @if ($errors->any())
                        <div class="col-sm-12  ">
                            <p-message :closable="false" severity="error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </p-message>
                        </div>
                @endif

                @if(session()->has('success'))
                    <div class="col-sm-12 ">
                        <p-message :closable="false" :sticky="false" :life="3000" severity="success">Guardado correctamente</p-message>
                    </div>
                @endif
                <div class="col-sm-6">
                    <span style="color: #b32d23"><h2>EG-@{{f.folio}}</h4></span>
                    </div>
                <div class="col-sm-6 text-right  switch-label">
                    <span class="p-float-label" style="margin-top: 0px;">
                        <p-switch name="paid" true-value="on" false-value="off"  id="paid" :model-value="true" v-model="f.form.paid" ></p-switch>
                        <label class="employee-label-switch" :class="{'active-employee': f.form.paid == 'on', 'inactive-employee': f.form.paid != 'on'}" for="account"> @{{f.form.paid == 'on' ? 'Pagado' : 'Por cobrar'}}</label>
                    </span>
                    </span>
                </div>
           
            <div class="col-sm-2 ">
                <span class="p-float-label">
                    <p-input name="reference"  id="reference" type="text" v-model="f.form.reference" ></p-input>
                    <label for="reference">Referencia</label>
                </span>
            </div>
                <div class="col-sm-4">
                    <span class="p-float-label">
                        <p-calendar   name="date" id="date" show-button-bar  name="date" label="Fecha" v-model="f.form.date"></p-calendar>
                        <label for="date">Fecha </label>
                    </span>
                </div>
                <div class="col-sm-12"></div>
                <div class="col-sm-6">
                    <span class="p-float-label">
                        <p-input name="categories"  id="categories" type="hidden" v-model="f.form.categories" ></p-input>
                        <p-multiselect option-value="id" name="categories" filter v-model="f.form.categories" :options="f.categories" option-label="name"></p-multiselect>
                        <label for="categories">Categoría</label>
                    </span>
                    <div class="d-flex aling-items-center add-button">
                        <p-button label="Agregar categoria" @click="f.opensidebar" icon="pi pi-plus" class="p-button-text" ></p-button>
                    </div>

                </div>
                <div class="col-sm-6">
                    <span class="p-float-label">
                        <p-input name="providers"  id="providers" type="hidden" v-model="f.form.providers" ></p-input>

                        <p-dropdown option-value="id" name="providers" filter v-model="f.form.providers" :options="f.providers" option-label="name"></p-dropdown>
                        <label for="providers">Proveedor</label>
                    </span>
                    <div class="d-flex aling-items-center add-button">
                        <p-button label="Agregar proveedor" @click="f.openproviders" icon="pi pi-plus" class="p-button-text" ></p-button>
                    </div>
                </div>
                <div class="col-sm-12 ">
                    <span class="p-float-label">
                        <p-textarea rows="5" cols="30" name="description"  id="description" type="text" v-model="f.form.description" ></p-textarea>
                        <label for="description">Descripción</label>
                    </span>
                </div>
                <div class="col-sm-3 ">
                    <span class="p-float-label">
                        <p-inputnumber currency="USD" name="cash" id="locale-us" v-model="f.form.cash" mode="decimal" locale="en-US" :min-fraction-digits="2"></p-inputnumber>
                        <label for="cash">Total en efectivo</label>
                    </span>
                </div>
                <div class="col-sm-3 ">
                    <span class="p-float-label">
                        <p-inputnumber currency="USD" name="card" id="locale-us" v-model="f.form.card" mode="decimal" locale="en-US" :min-fraction-digits="2"></p-inputnumber>
                        <label for="card">Total en tarjeta</label>
                    </span>
                </div>
                <div class="col-sm-12 mt-5 text-right">
                    <span class="total-amount">Total $ @{{f.total}}</span>
                </div>
            <div class="col-sm-12 text-right mt-4">
            
                @if(has_permission('bills_delete'))
                <p-button @click="f.deletecompany" v-if="f.form.id" class=" mr-2 p-button-danger" label="Eliminar"></p-button>
               @endif
               @if(has_permission('bills_create'))
               <p-button v-if="f.form.id == null" type="submit" class="p-button-success" label="Guardar"  ></p-button>
               @endif
               @if(has_permission('bills_edit'))
               <p-button v-if="f.form.id" type="submit" class="p-button-success" label="Guardar"  ></p-button>
               @endif
            </div>
        </form>
        </template>
    </card>
   </bills-form>

</x-app-layout>
