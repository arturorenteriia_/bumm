<x-app-layout>
                <div class="full-w">
                    <div class="p-breadcrumb p-component">
                        <ul >
                            <li class="p-breadcrumb-home">
                                <a href="/dashboard" class="p-menuitem-link">
                                <span class="p-menuitem-icon pi pi-home"></span></a>
                            </li>
                            <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                            <li>
                                <span class="p-menuitem-text">Egresos</span>
                            </li>
                        
                        </ul>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-6 mt-3 text-center ">
                        <card-title title="Resumen de egresos" color="red"></card-title>
                    </div>
                    <div class="col-sm-6 mt-3">
                        <card class="no-shadow br-20">
                            <template #content>
                                @if(has_permission('bills_create'))
                                    <div style="display: grid;">
                                        <p-button @click="openUrl('/bills/bill')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                        <small class="mt-2">Agregar</small>
                                    </div>
                                @endif
                            </template>
                        </card>
                    </div>
                   
                </div>
                <bill-index :total="{{$total}}" :bills="{{json_encode($bills)}}" v-slot="b">
                    <card class="no-shadow mb-3">
                        <template #content>
                            <div  class="form-properties row mb-3">
                                <div class="col-sm-12 mt-2">
                                    <span ><strong>@{{b.search.requested}}</strong></span>
                                </div>
                                <div class="col-sm-3 ">
                                    <span class="p-float-label">
                                        <p-input   name="ref" id="ref" show-button-bar  name="ref" label="Referencia" v-model="b.search.reference"></p-input>
                                        <label for="ref">Folio o Referencia</label>
                                    </span>
                                </div>
                                <div class="col-12 col-md-4">
                                    <span class="p-float-label">
                                        <p-dropdown :show-clear="true" option-value="id" name="providers" filter v-model="b.search.provider" :options="{{$providers}}" option-label="name"></p-dropdown>
                                        <label for="clients">Proveedor</label>
                                    </span>
                                </div>
                                <div class="col-12 col-md-4">
                                    <span class="p-float-label">
                                        <p-dropdown :show-clear="true" option-value="id" name="categories" filter v-model="b.search.category" :options="{{$categories}}" option-label="name"></p-dropdown>
                                        <label for="clients">Categoria</label>
                                    </span>
                                </div>
                                <div class="col-sm-3 ">
                                        <span class="p-float-label">
                                            <p-calendar   name="start" id="start" show-button-bar  name="start" label="Fecha" v-model="b.search.start"></p-calendar>
                                            <label for="start">Fecha inicial</label>
                                        </span>
                                </div>
                                <div class="col-sm-3">
                                    <span class="p-float-label">
                                        <p-calendar   name="end" id="end" show-button-bar  name="end" label="Fecha" v-model="b.search.end"></p-calendar>
                                        <label for="end">Fecha Final</label>
                                    </span>
                                </div>
                                <div class="col-sm-3 text-right text-md-left col-md-3 form-margin">
                                        <p-button @click="b.searchbydate" label="Buscar" icon="pi pi-search" class="p-button-sm"></p-button>
                                </div>
                            </div>

                        </template>
                    </card>
                    
                    <card class="no-shadow mb-3 ">
                        <template #content>
                            <div class="row ">
                                <div class="col-sm-12  mb-3">
                                    
                                    <data-table responsive-layout="scroll" :paginator="true" :rows="10" class="p-datatable-sm" :value="b.allbills" responsiveLayout="scroll">
                                        <column field="folio" header="Folio">
                                            <template #body="slotProps">
                                                <span style="color: #b32d23" v-if="slotProps.data.folio">IN-@{{slotProps.data.folio.padStart(6, "0")}}</span>
                                            </template>
                                        </column>
                                        <column :sortable="true" field="reference" header="Referencia"></column>
                                        <column :sortable="true" field="categories_names" header="Categorias"></column>
                                        <column :sortable="true" field="providers_names" header="Provedor"></column>
        
                                        <column :sortable="true" field="total" header="Total">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.total)}}                       
                                            </template>
                                        </column>
                                        <column :sortable="true" field="date" header="Fecha"></column>
                                        <column :sortable="true" field="paid" header="Estatus">
                                            <template #body="slotProps">
                                                <p-badge v-if="slotProps.data.paid == 1" value="Pagado" class="success-chip"> </p-badge>
                                                <p-badge v-else value="No Pagado" class="danger-chip"> </p-badge>                        </template>
                                        </column>
                                        <column field="action" header="Opciones">
                                            <template #body="slotProps">
                                                <p-button v-tooltip.left="'Ver egreso'" @click="openUrl('/bills/bill/' + slotProps.data.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                                            </template>
                                        </column>
                                    </data-table>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="d-flex mt-2 aling-items-center add-button">
                                                <p-button  @click="openUrl('/reports/bills')" label="Buscar egresos con filtros avanzados" icon="pi pi-search" class="p-button-text" ></p-button>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mt-3 text-right"><strong>Total egresos: $@{{currencyFormat(b.search.total)}}</strong></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </template>
                    </card>
                    <card class="no-shadow"> 
                        <template #content>
                            <div class="row mt-5">
                                <div class="col-sm-8"></div>
                                <div class="col-sm-4">
                                    <p-dropdown @change="b.changerange" option-value="value" v-model="b.range.current" :options="b.range.options" option-label="label" ></p-dropdown>
                                </div>
                                <div class="col-sm-12 ">
                                    <p-chart type="line" :data="b.chartdata" :options="b.chartoptions"></p-chart>
                                    <div class="mt-3 text-right"><strong>Total egresos: $@{{currencyFormat(b.currenttotal)}}</strong></div>
                                </div>
                            </div>
                        </template>
                    </card>
                </bill-index>



</x-app-layout>
