<x-app-layout>
    <meta name="csrf-token" content=" {{ csrf_token() }}">
            <div class="full-w mb-3">
                <div class="p-breadcrumb p-component mb-3">
                    <ul >
                        <li class="p-breadcrumb-home">
                            <a href="/dashboard" class="p-menuitem-link">
                            <span class="p-menuitem-icon pi pi-home"></span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <a href="/config" class="p-menuitem-link">
                            <span class="p-menuitem-text">Configuracion</span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <span class="p-menuitem-text">Configuración del negocio</span>
                        </li>
                    </ul>
                </div>
               </div>
               <company-profile :business="{{Auth::user()->business}}" v-slot="p">
                    <div class="row">
                        <div class="col-sm-12">
                            <card-title class="" title="Datos de tu empresa" :filter="true" color="white"></card-title>
                        </div>
                            <div class="col-sm-12 mt-3">
                                <card class="no-shadow">
                                    <template #content>
                                        <div class="row">
                                            <div class="col-sm-8 center-logo-title">
                                                <h2>Elige el logo de tu empresa</h2>
                                            </div>
                                            <div class="col-sm-4 justify-end">
                                                <p-image imageClass="max-height-logo" :src=" p.data.business.pic ?  '/images/profile-pics/'+ p.data.business.pic : '/images/insert_logo.png'" 
                                                alt="Image" height="100" width="140" :preview="p.data.business.pic ? true : false"></p-image>
                                            <p-fileupload class="mt-2" mode="basic" 
                                                    name="demo[]" url="/ajax/upload-profile-pic" 
                                                    accept="image/*" :max-file-size="1000000" 
                                                     :auto="true" 
                                                     @upload="p.upload"
                                                    choose-label="Cambiar logo">
                                                </p-fileupload>
                                            </div>
                                        </div>
                                    </template>
                                </card>
                            </div>
                            <div class="col-sm-12 mt-4">
                                <card class="no-shadow">
                                    <template #content>
                                        @if(session()->has('success'))
                                        <div >
                                            <p-message :closable="false" :sticky="false" :life="3000" severity="success">Guardado correctamente</p-message>
                                        </div>
                                    @endif
                                        <form  action="/config/business/save" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row form-properties">
                                            <div class="col-sm-12 ">
                                                <span class="p-float-label">
                                                    <p-input name="name"  id="reference" type="text" v-model="p.data.business.name" ></p-input>
                                                    <label for="reference">Nombre</label>
                                                </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <span class="p-float-label">
                                                    <p-input name="address"  id="reference" type="text" v-model="p.data.business.address" ></p-input>
                                                    <label for="reference">Dirección</label>
                                                </span>
                                            </div>
                                            <div class="col-sm-4 ">
                                                <span class="p-float-label">
                                                    <p-input name="phone"  id="reference" type="text" v-model="p.data.business.phone" ></p-input>
                                                    <label for="reference">Teléfono</label>
                                                </span>
                                            </div>
                                            <div class="col-sm-4 ">
                                                <span class="p-float-label">
                                                    <p-input name="email"  id="reference" type="text" v-model="p.data.business.email" ></p-input>
                                                    <label for="reference">E-mail</label>
                                                </span>
                                            </div>
                                            <div class="col-sm-12 mt-3 text-right">
                                                <p-button type="p.submit" class="p-button-success" label="Guardar"></p-button>
                                            </div>
                                        </div>
                                        </form>
                                    </template>
                                </card>
                            </div>
                    </div>
            </company-profile>
       

</x-app-layout>
