<x-app-layout>
    <div class="full-w mb-3">
            <div class="p-breadcrumb p-component mb-3">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/config" class="p-menuitem-link">
                        <span class="p-menuitem-text">Configuracion</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Empleados</span>
                    </li>
                
                </ul>
            </div>
        </div>
    <config-employees-index  v-slot="b" :employees="{{json_encode($employees)}}">
        <div class="row mb-3">
            <div class="col-sm-6 mt-3 text-center ">
               <card-title title="Resumen de empleados"  color="blue"></card-title>
            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission(['config_employees_edit', 'config_employees_edit_accounts']))
                            <div style="display: grid">
                                <p-button  @click="openUrl('/config/employee')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>

        <div class="row links-card">
        </div>
        <div class="row">
            <div class="col-sm-12">
                <card class="no-shadow">
                    <template #content>
                        <div class="row">
                            <div class="col-sm-4">
                                <span class="p-float-label">
                                    <p-input   name="name" id="name" show-button-bar v-model="b.search.name" name="name" label="Nombre" ></p-input>
                                    <label for="name">Nombre, E-mail o Telefono</label>
                                </span>
                            </div>
                            <div class="col-12 col-md-3 text-right text-md-left">
                                <p-button @click="b.searchbyname" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                            </div>
                        </div>
                    </template>
                </card>
            </div>
            <div class="col-sm-12 mt-3">
                 <card class="no-shadow">
                    <template #content>
                        <data-table responsive-layout="scroll" :paginator="true" :rows="10" class="p-datatable-sm" :value="b.allemployees" responsiveLayout="scroll">
                            <column :sortable="true" field="name" header="Nombre"></column>
                            <column :sortable="true" field="last_name" header="Apellido"></column>
                            <column :sortable="true" field="active" header="Estatus">
                               <template #body="slotProps">
                                   <p-badge v-if="slotProps.data.active == 1" value="Activo" class="success-chip"> </p-badge>
                                   <p-badge v-else value="Inactivo" class="danger-chip"> </p-badge>
                               </template> 
                           </column>
                           <column :sortable="true" field="account" header="Acceso a plataforma">
                               <template #body="slotProps">
                                   <p-badge v-if="slotProps.data.account == 1" value="Activo" class="success-chip"> </p-badge>
                                   <p-badge v-else value="Inactivo" class="danger-chip"> </p-badge>               </template>
                           </column>
                            <column field="action" header="Opciones">
                                <template #body="slotProps">
                                    <p-button v-tooltip.left="'Ver Empleado'" @click="openUrl('/config/employee/' + slotProps.data.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                                </template>
                            </column>
                        </data-table>
                    </template>
                 </card>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12  text-right" >
                <div class="mt-3"><strong>Total de empleados: @{{b.allemployees.length}}</strong></div>
            </div>
        </div>
    </config-employees-index>
</x-app-layout>
