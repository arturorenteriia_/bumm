<x-app-layout>
    <div class="full-w mb-3">
        <div class="p-breadcrumb p-component mb-3">
            <ul >
                <li class="p-breadcrumb-home">
                    <a href="/dashboard" class="p-menuitem-link">
                    <span class="p-menuitem-icon pi pi-home"></span></a>
                </li>
                <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                <li>
                    <span class="p-menuitem-text">Configuración</span>
                </li>
            </ul>
        </div>
    </div>
    <card-title class="mt-3 mb-3" title="Configuración" color="blue"></card-title>
    <div class="row">
        @if(has_permission('config_company_profile'))
           
        <div class="options-card col-12 col-sm-6 col-md-4 mb-5 mouse" @click="openUrl('/config/company-profile')">
            <div class="header-options-card">
                <span>Cuenta y configuración</span>
            </div>
            <card class="no-shadow" style="height: 12rem;">
                <template #content>
                    <div class="row">
                        <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                        <div class="col-sm-12">
                            Configura tu negocio, logo, correo, nombre 
                        </div>
                    </div>
                </template> 
            </card>
        </div>
        @endif

        @if(has_permission('config_employees_access'))
        <div class="options-card col-12 col-sm-6 col-md-4 mb-5 mouse" @click="openUrl('/config/employees')">
            <div class="header-options-card">
                <span>Empleados</span>
            </div>
            <card class="no-shadow" style="height: 12rem;">
                <template #content>
                    <div class="row">
                        <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                        <div class="col-sm-12">
                            Agrega empleados a tu organización, podras darles acceso a la plataforma,
                                configurar su sueldo base o sus comisiones y su informacion personal. 
                        </div>
                    </div>
                </template> 
            </card>
            </div>
            @endif
            @if(has_permission('config_sales_access'))
            
            <div class="options-card col-12 col-sm-6 col-md-4 mb-5 mouse" @click="openUrl('/config/sales')">
                <div class="header-options-card">
                    <span>Categorias de ingresos</span>
                </div>
                <card class="no-shadow" style="height: 12rem;">
                    <template #content>
                        <div class="row">
                            <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                            <div class="col-sm-12">
                            Configura las categrias de los ingresos, por ejemplo "Servicio", "Producto",
                                    "Limpieza dental", "Prestamo bancario" etc. 
                            </div>
                        </div>
                    </template> 
                </card>
            </div>
            @endif
           
            @if(has_permission('config_bills_access'))
           
            <div class="options-card col-12 col-sm-6 col-md-4 mb-5 mouse" @click="openUrl('/config/bills')">
                <div class="header-options-card">
                    <span>Categorias de egresos</span>
                </div>
                <card class="no-shadow" style="height: 12rem;">
                    <template #content>
                        <div class="row">
                            <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                            <div class="col-sm-12">
                                Configura las categrias de los egresos, por ejemplo "Renta", "Luz",
                                "Devolución", "Nomina" etc. 
                            </div>
                        </div>
                    </template> 
                </card>
            </div>
            @endif

           

    </div>
</x-app-layout>