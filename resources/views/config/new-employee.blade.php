<x-app-layout>
           <div class="full-w">
            <div class="p-breadcrumb p-component">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/config" class="p-menuitem-link">
                        <span class="p-menuitem-text">Configuracion</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/config/employees" class="p-menuitem-link">
                        <span class="p-menuitem-text">Empleados</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Agregar / Editar</span>
                    </li>
                
                </ul>
            </div>
           </div>
       <employee-form v-slot="f" employee="{{count(request()->old()) > 0 ? json_encode(request()->old()) : (Route::currentRouteName() == "update-employee" ? json_encode($employee) : json_encode([]))}}">
        <div v-if="f.form.id" class="row mb-3">
            <div class="col-sm-6 mt-3 text-center ">
                <card-title title="Información del empleado"  :filter="true" color="white"></card-title>
            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission('bills_create'))
                            <div style="display: grid">
                                <p-button @click="openUrl('/config/employee')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>
        <card-title class="mt-3 mb-3 " v-else title="Agregar nuevo empleado"  :filter="true" color="white"></card-title>
                <card class="no-shadow">
                <template #content>
                    <form action="/config/save-employee" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <p-input  name="id" id="id" type="hidden" v-model="f.form.id" ></p-input>
                    <div class="row form-properties " style="position: relative;">
                    
                        @if ($errors->any())
                        <div class="ml-3">
                            <p-message :closable="false" severity="error">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </p-message>
                        </div>
                    @endif
                    @if(session()->has('success'))
                        <div class="col-sm-12">
                            <p-message :closable="false" :sticky="false" :life="3000" severity="success">Guardado correctamente</p-message>
                        </div>
                    @endif
                    <div class="col-sm-12 mt-5 text-right  switch-label">
                        <span class="p-float-label" style="margin-top: 0px;">
                            <p-switch name="active" true-value="on" false-value="off"  id="account" :model-value="true" v-model="f.form.active" ></p-switch>
                            <label class="employee-label-switch" :class="{'active-employee': f.form.active == 'on', 'inactive-employee': f.form.active != 'on'}" for="account">Empleado @{{f.form.active == 'on' ? 'activo' : 'inactivo'}}</label>
                        </span>
                        </span>
                    </div>
                        <div class="col-sm-12 mt-5">
                            <h4 style="margin: 0px">Datos personales</h4>
                        </div>
                        <div class="col-sm-3 ">
                            <span class="p-float-label">
                                <p-input  name="name" id="name" type="text" v-model="f.form.name" ></p-input>
                                <label for="name">Nombres</label>
                            </span>
                        </div>
                        <div class="col-sm-3 ">
                            <span class="p-float-label">
                                <p-input name="last_name" id="lastname" type="text" v-model="f.form.last_name" ></p-input>
                                <label for="lastname">Apellidos</label>
                            </span>
                        </div>
                        <div class="col-sm-6"></div>
                        <div class="col-sm-4 ">
                            <span class="p-float-label">
                                <p-input name="email" id="email" type="text" v-model="f.form.email" ></p-input>
                                <label for="email">E-mail</label>
                            </span>
                        </div>
                        <div class="col-sm-4 ">
                            <span class="p-float-label">
                                <p-input name="phone"  id="phone" type="text" v-model="f.form.phone" ></p-input>
                                <label for="phone">Telefono</label>
                            </span>
                        </div>
                        <div class="col-sm-4 ">
                            <span class="p-float-label">
                                <p-input name="address"  id="address" type="text" v-model="f.form.address" ></p-input>
                                <label for="address">Direccion</label>
                            </span>
                        </div>
                        <div class="col-sm-4 ">
                            <span class="p-float-label">
                                <p-input name="city" id="city" type="text" v-model="f.form.city" ></p-input>
                                <label for="city">Ciudad</label>
                            </span>
                        </div>
                        <div class="col-sm-4 ">
                            <span class="p-float-label">
                                <p-input name="zipcode"  id="zipcode" type="text" v-model="f.form.zipcode" ></p-input>
                                <label for="zipcode">Codigo postal</label>
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <span class="p-float-label">
                                <p-input name="state_id"  id="state_id_hidden" type="hidden" v-model="f.form.state_id" ></p-input>
                                <p-dropdown id="state_id" :filter="true" v-model="f.form.state_id" :options="$root.$data.states" option-label="name" option-value="code" ></p-dropdown>                            <label for="state">Estado</label>
                            </span>
                        </div>
                        <div class="col-sm-4 flex justify-center items-center">
                            <span class="p-float-label">
                                <p-calendar   name="birth" id="date" show-button-bar  name="date" label="Fecha" v-model="f.form.birth"></p-calendar>
                                <label for="date">Fecha de nacimiento</label>
                            </span>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <h4>Esquema de pagos</h4>
                        </div>
                        <div class="col-sm-3">
                                <div class="field-radiobutton">
                                    <p-radio name="payment_type"  id="paymentType"  class="mr-2" value="commission"  v-model="f.form.payment_type"></p-radio>
                                    <label for="paymentType">Comision</label>
                                </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="field-radiobutton">
                                <p-radio class="mr-2" id="paymentType" name="payment_type" value="salary"  v-model="f.form.payment_type"></p-radio>
                                <label for="salary">Sueldo Base</label>
                            </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="field-radiobutton">
                            <p-radio class="mr-2" id="mixed" name="payment_type" value="mixed"  v-model="f.form.payment_type"></p-radio>
                            <label for="mixed">Comision y Sueldo base</label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="field-radiobutton">
                            <p-radio class="mr-2" id="none" name="payment_type" value="none"  v-model="f.form.payment_type"></p-radio>
                            <label for="none">Ninguno</label>
                        </div>
                    </div>
                    <div class="col-sm-4" v-if="f.form.payment_type == 'commission' || f.form.payment_type == 'mixed'">
                        <span class="p-float-label">
                            <p-input name="commission"   id="input-commission" type="text"  v-model="f.form.commission"></p-input>
                            <label for="input-commission">Comisión %</label>
                        </span>
                    </div>
                    <div class="col-sm-4" v-if="f.form.payment_type == 'salary' || f.form.payment_type == 'mixed'">
                        <span class="p-float-label">
                            <p-input name="salary" default  id="input-salary"  type="text" v-model="f.form.salary"></p-input>
                            <label for="input-salary"> Sueldo</label>
                        </span>
                    </div>
                    <p-switch v-show="false" name="account" true-value="on" false-value="off" id="account" :model-value="true" v-model="f.form.account" ></p-switch>
                    <p-input v-show="false" name="username" :disabled="f.form.account !== 'on'"  id="username" type="text" v-model="f.form.username"></p-input>
                    @if(has_permission('config_employees_edit_accounts'))
                    <div class="col-sm-12 mt-4">
                        <h4>Acceso a plataforma</h4>
                    </div>
                    <div class="col-sm-12  switch-label">
                        <span class="p-float-label" style="margin-top: 0px">
                            <p-switch name="account" true-value="on" false-value="off" id="account" :model-value="true" v-model="f.form.account" ></p-switch>
                            <label for="account">Activar</label>
                        </span>
                        </span>
                    </div>
                    <div class="col-sm-4">
                        <span class="p-float-label">
                            <p-input name="username" :disabled="f.form.account !== 'on'"  id="username" type="text" v-model="f.form.username"></p-input>
                            <label for="username">Nombre de usuario</label>
                        </span>
                    </div>
                    <div class="col-sm-4">
                        <span class="p-float-label">
                            <p-input name="password" :disabled="f.form.account !== 'on'" id="pass"  type="password" v-model="f.form.password1"></p-input>
                            <label for="pass">Contraseña</label>
                        </span>
                    </div>
                    <div class="col-sm-4">
                        <span class="p-float-label">
                            <p-input name="password_confirm" :disabled="f.form.account !== 'on'" id="username"  type="password" v-model="f.form.password2"></p-input>
                            <label for="username">Repetir Contraseña</label>
                        </span>
                    </div>
                    @if(Auth::user()->employee_id !== $employee->id)
                    <div class="col-sm-12 mt-5" v-if="f.form.account == 'on'">
                        <card>
                            <template #content>
                               <div class="row">
                                    <div class="col-sm-12">
                                        <h4>Permisos de plataforma</h4>
                                        <p-input name="permissions"  type="hidden" v-model="f.permissions[0].model"></p-input>
                                    </div>
                                    <div v-for="group in f.permissions[0].permissions" class="col-sm-12 mt-4 mb-4">
                                       <span style="font-size: 1.5rem;
                                       font-weight: bolder;">@{{group.group}}</span>
                                       <div class="row form-properties">
                                           <div class="col-sm-3 mt-3" v-for="p in group.permissions">
                                            <span class="p-float-label" style="margin-top: 0px">
                                                <p-checkbox 
                                                :name="p.permission"
                                                    :value="p.permission" :id="p.permission"  
                                                    v-model="f.permissions[0].model" 
                                                    :model-value="f.permissions[0].model" >
                                                </p-checkbox>
                                                <label style="margin-left: 1.2rem" 
                                                    for="account">@{{p.name}}
                                                </label>
                                            </span>
                                           </div>
                                       </div>
                                    </div>
                               </div>
                            </template>
                        </card>
                    </div>
                    @endif
                    @endif
                    <div class="col-sm-12 text-right mt-4">
                        @if(has_permission(['config_employees_edit', 'config_employees_edit_accounts']))
                        <p-button @click="f.deleteemployee" v-if="f.form.id" class=" mr-2 p-button-danger" label="Eliminar"></p-button>
                        <p-button type="submit" class="p-button-success" label="Guardar"  ></p-button>
                        @endif
                    </div>
                    </div>
                </form>
                </template>
                </card>
       </employee-form>

</x-app-layout>
