<x-app-layout>
            <div class="full-w mb-3">
                <div class="p-breadcrumb p-component mb-3">
                    <ul >
                        <li class="p-breadcrumb-home">
                            <a href="/dashboard" class="p-menuitem-link">
                            <span class="p-menuitem-icon pi pi-home"></span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <a href="/config" class="p-menuitem-link">
                            <span class="p-menuitem-text">Configuracion</span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <span class="p-menuitem-text">Categoria de Ingresos</span>
                        </li>
                    
                    </ul>
                </div>
               </div>
       <categories  :categories="{{json_encode($categories)}}" v-slot="categories">
        
        
        <div class="row">
            <div class="col-sm-6 mt-3 text-center ">
                <card-title title="Categorias de ingresos"  :filter="false" color="blue"></card-title>
            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission('bills_create'))
                            <div style="display: grid">
                                <p-button  @click="categories.showform" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>
                <card class="no-shadow mt-3">
                    <template #content>
                        <div class="row">
                            <div class="col-sm-12 ">
                                <data-table responsive-layout="scroll" :paginator="true" :rows="10" class="p-datatable-sm" :value="categories.categories" responsiveLayout="scroll">
                                    <column :sortable="true" field="name" header="Nombre"></column>
                                    <column field="action" header="Opciones" >
                                        <template #body="slotProps" >
                                            <p-button :disabled="slotProps.data.blocked" v-tooltip.left="'Editar'" @click="categories.showform(slotProps.data)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-pencil" iconPos="right"></p-button>
                                            <p-button :disabled="slotProps.data.blocked" v-tooltip="'Eliminar'" @click="(event) => categories.deletecategory(slotProps, event) (slotProps)" class="p-button-danger p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-trash" iconPos="right"></p-button>
                                        </template>
                                    </column>
                                </data-table>
                            </div>
                        </div>
                    </template>
                </card>
       </categories>

</x-app-layout>
