<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="{{ mix('css/main.css') }}">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <link rel="stylesheet" href="{{ '/admin/gilroy.css' }}">
    <body class="font-sans antialiased">

            <!-- Page Content -->
            <main id="app" v-cloak class="min-h-screen w-full">
                <div class="" style="width: 100%; min-height: 100vh; position: relative ">
                <div v-if="isLoadingScreen" class="loading-spinner">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <i class="pi pi-spin pi-spinner" style="font-size: 2rem"></i>
                        </div>
                        <div class="col-sm-12 text-center">
                            Cargando...
                        </div>
                    </div>
                </div>
                <p-block :blocked="isLoadingScreen" :full-screen="true">
                        <aside id="menu-sidebar" class="menu" :class="{'menu-sidebar-mobile': !css.openMenu}">
                            <card class="h-full ">
                                <template #content>
                                    <div class="row menu-burguer" v-if="breakpoint.mobile.matches">
                                        <div class="col-sm-12 text-right">
                                            <p-button style="color: white;
                                            margin-left: 1rem;" @click="fullMenu()" icon="pi pi-bars" class="p-button-danger p-button-text"></p-button>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center mb-5">
                                        <img style="width: 50%" src="{{ asset('images/logo-ongesti.png') }}" alt="">
                                    </div>
                                    

                                    <span  class="full-w p-input-icon-left">
                                        <i class="pi pi-search"></i>
                                        <p-input type="text" placeholder="Buscar"></p-input>
                                    </span>
                                  
                                        <div class="mt-2">
                                            @if(has_permission('dashboard_access'))
                                            <a href="/dashboard" >
                                                <div  class="menu-container @if(Request::is('dashboard')) active-menu @endif}}">
                                                    Dashboard
                                                </div>
                                            </a>
                                           @endif
                                           @if(has_permission('sales_access'))
                                                <div @click="toggle"  class="menu-container @if(Request::is('sales') || Request::is('sales/*')) active-menu @endif}}">
                                                Ventas <i  class="icon-to-right pi pi-angle-down"></i>
                                                </div>
                                                <p-tieredmenu id="menu" ref="menu" :model="salesItemsMenu" :popup="true"></p-tieredmenu>

                                           @endif
                                           @if(has_permission('bills_access')) 
                                           <a href="/bills">
                                                <div  class="menu-container @if(Request::is('bills') || Request::is('bills/*')) active-menu @endif}}">
                                                    Egresos
                                                </div>
                                            </a>
                                            @endif
                                            @if(has_permission('clients_access'))
                                            <a href="/clients">
                                                <div  class="menu-container @if(Request::is('clients') || Request::is('clients/*')) active-menu @endif}}">
                                                    Clientes
                                                </div>
                                            </a>
                                            @endif
                                            @if(has_permission('providers_access'))
                                            <a href="/providers">
                                                <div  class="menu-container @if(Request::is('providers') || Request::is('providers/*')) active-menu @endif}}">
                                                    Proveedores
                                                </div>
                                            </a>
                                            @endif
                                            @if(has_permission('reports_access'))
                                            <a href="/reports">
                                                <div  class="menu-container @if(Request::is('reports/*') || Request::is('reports')) active-menu @endif}}">
                                                    Reportes
                                                </div>
                                            </a>
                                            @endif
                                            @if(has_permission('config_access'))
                                            <a href="/config">
                                                <div  class="menu-container @if(Request::is('config/*') || Request::is('config')) active-menu @endif}}">
                                                    Configuración
                                                </div>
                                            </a>
                                            @endif
                                            <hr>
                                            <a href="/logout">
                                                <div  class="menu-container">
                                                    Cerrar sesion
                                                </div>
                                            </a>
                                        </div>
                                </template>
                            </card>
                        </aside>
                       
                        <div class="main-container" :class="{'main-container-mobile': css.fullMainContainer}">
                            <div class="menu-burguer header-container">
                                <div class="row full-w">
                                    <div class="col-4">
                                        <p-button  style="margin-left: 1rem;" @click="fullMenu()" icon="pi pi-bars" 
                                        class="p-button p-button-text main-blue-color"></p-button>
                                    </div>
                                    <div class="col-8 text-right">
                                        <p-button icon="pi pi-user" class="radius-rounded p-button-rounded p-button-outlined main-blue-color"></p-button>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 pl-3 p-2 p-lg-5 ">
    
                                        {{$slot}}
                                    </div>
                                   
                                   
                                </div>
                            </div>
                        </div>
                        <p-alert position="top-right"></p-alert>
                </p-block>
            </div>
         </main>
        
    </body>
</html>