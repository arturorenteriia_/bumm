<x-app-layout>
        <div class="full-w">
            <div class="p-breadcrumb p-component">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Proveedores</span>
                    </li>
                
                </ul>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-6 mt-3 text-center ">
                <card-title title="Resumen de proveedores" color="blue"></card-title>
            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission('providerscreate'))
                            <div style="display: grid">
                                <p-button @click="openUrl('/providers/provider')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>
    <companies-index  v-slot="b" :clients="{{json_encode($providers)}}">
        <card class="no-shadow mb-3">
            <template #content>
                <div class="row">
                    <div class="col-sm-4 mb-3">
                        <span class="p-float-label">
                            <p-input   name="name" id="name" show-button-bar  name="name" label="Nombre" v-model="b.search.name"></p-input>
                            <label for="name">Nombre, RFC o Razón Social</label>
                        </span>
                    </div>
        
                    <div class="col-12 col-md-3  text-right text-md-left">
                        <p-button @click="b.searchbyname" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                    </div>
                </div>
            </template>
        </card>
        <card class="no-shadow mb-3">
            <template #content>
                <div class="row">
                    <div class="col-sm-12 mt-3">
                         <data-table responsive-layout="scroll" :paginator="true" :rows="10" class="p-datatable-sm" :value="b.allclients" responsiveLayout="scroll">
                             <column :sortable="true" field="name" header="Nombre"></column>
                             <column :sortable="true" field="rfc" header="RFC"></column>
                             <column :sortable="true" field="legal_name" header="Razón Social">
                            </column>
                            <column :sortable="true" field="active" header="Estatus">
                                <template #body="slotProps">
                                    <p-badge v-if="slotProps.data.active == 1" value="Activo" class="success-chip"> </p-badge>
                                    <p-badge v-else value="Inactivo" class="danger-chip"> </p-badge>
                                </template> 
                            </column>
                             <column field="action" header="Opciones">
                                 <template #body="slotProps">
                                     <p-button v-tooltip.left="'Ver Cliente'" @click="openUrl('/providers/provider/' + slotProps.data.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                                 </template>
                             </column>
                         </data-table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12  text-right" >
                        <div class="mt-3"><strong>Total de proveedores: @{{b.allclients.length}}</strong></div>
                    </div>
                </div>
            </template>
        </card>
    </companies-index>
</x-app-layout>
