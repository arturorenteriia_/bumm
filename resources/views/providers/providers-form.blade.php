<x-app-layout>
            <div class="full-w">
                <div class="p-breadcrumb p-component">
                    <ul >
                        <li class="p-breadcrumb-home">
                            <a href="/dashboard" class="p-menuitem-link">
                            <span class="p-menuitem-icon pi pi-home"></span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <a href="/providers" class="p-menuitem-link">
                            <span class="p-menuitem-text">Proveedores</span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <span class="p-menuitem-text">Agregar / Editar</span>
                        </li>
                    
                    </ul>
                </div>
        </div>
       <company-form   v-slot="f" company="{{count(request()->old()) > 0 ? json_encode(request()->old()) : (Route::currentRouteName() == "update-provider" ? json_encode($provider) : json_encode([]))}}">
        <div v-if="f.form.id" class="row mb-3">
            <div class="col-sm-6 mt-3 text-center ">
                <card-title  v-else title="Información de proveedor"  :filter="true" color="white"></card-title>

            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission('providers_create'))
                            <div style="display: grid">
                                <p-button @click="openUrl('/providers/provider')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>
        <card-title class="mt-3 mb-3 " v-else title="Agregar nuevo proveedor"  :filter="true" color="white"></card-title>

        <card class="no-shadow">
            <template #content>
                
                <form action="/clients/save-company" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <p-input  name="id" id="id" type="hidden" v-model="f.form.id" ></p-input>
                    <p-input  name="company_type" id="company_type" type="hidden" model-value="provider" ></p-input>
                <div class="row form-properties " style="position: relative;">
                    
                    @if ($errors->any())
                            <div class="col-sm-12">
                                <p-message :closable="false" severity="error">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </p-message>
                            </div>
                    @endif

                    @if(session()->has('success'))
                        <div class="col-sm-12">
                            <p-message :closable="false" :sticky="false" :life="3000" severity="success">Guardado correctamente</p-message>
                        </div>
                    @endif

                    @if(session()->has('cant-edit'))
                        <div class="col-sm-12">
                            <p-message :closable="false" :sticky="false" :life="3000" severity="error">Este proveedor no se puede editar ni eliminar porque es generado por el sistema.</p-message>
                        </div>
                    @endif

                <div class="col-sm-12 text-right mt-4  switch-label">
                    <span class="p-float-label" style="margin-top: 0px;">
                        <p-switch name="active" true-value="on" false-value="off"  id="account" :model-value="true" v-model="f.form.active" ></p-switch>
                        <label class="employee-label-switch" :class="{'active-employee': f.form.active == 'on', 'inactive-employee': f.form.active != 'on'}" for="account">Proveedor @{{f.form.active == 'on' ? 'activo' : 'inactivo'}}</label>
                    </span>
                    </span>
                </div>
                    <div class="col-sm-4">
                        <span class="p-float-label">
                            <p-input  name="name" id="name" type="text" v-model="f.form.name" ></p-input>
                            <label for="name">Nombre </label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="rfc" id="lastname" type="text" v-model="f.form.rfc" ></p-input>
                            <label for="rfc">RFC</label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="legal_name" id="legal_name" type="text" v-model="f.form.legal_name" ></p-input>
                            <label for="legal_name">Razón Social</label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="email" id="email" type="text" v-model="f.form.email" ></p-input>
                            <label for="email">E-mail</label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="phone"  id="phone" type="text" v-model="f.form.phone" ></p-input>
                            <label for="phone">Telefono</label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="address"  id="address" type="text" v-model="f.form.address" ></p-input>
                            <label for="address">Direccion</label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="city" id="city" type="text" v-model="f.form.city" ></p-input>
                            <label for="city">Ciudad</label>
                        </span>
                    </div>
                    <div class="col-sm-4 ">
                        <span class="p-float-label">
                            <p-input name="zipcode"  id="zipcode" type="text" v-model="f.form.zipcode" ></p-input>
                            <label for="zipcode">Codigo postal</label>
                        </span>
                    </div>
                    <div class="col-sm-4">
                        <span class="p-float-label">
                            <p-input name="state_id"  id="state_id_hidden" type="hidden" v-model="f.form.state_id" ></p-input>
                            <p-dropdown id="state_id" :filter="true" v-model="f.form.state_id" :options="$root.$data.states" option-label="name" option-value="code" ></p-dropdown>
                            {{-- <p-input name="state_id" id="state" type="text" v-model="f.form.state_id" ></p-input> --}}
                            <label for="state">Estado</label>
                        </span>
                    </div>
                    <div class="col-sm-3 mt-3">
                        <div class="field-radiobutton">
                            <p-radio class="mr-2" id="moral" name="legal_type" value="moral"  v-model="f.form.legal_type"></p-radio>
                            <label for="moral">Persona Moral</label>
                        </div>
                    </div>
                    <div class="col-sm-3 mt-3">
                        <div class="field-radiobutton">
                            <p-radio class="mr-2" id="fisica" name="legal_type" value="fisica"  v-model="f.form.legal_type"></p-radio>
                            <label for="fisica">Persona Fisica</label>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-3">
                        <span class="p-float-label">
                            <p-textarea name="notes"  id="notes" type="text" v-model="f.form.notes" ></p-textarea>
                            <label for="notes">Notas</label>
                        </span>
                    </div>
                
                <div class="col-sm-12 text-right mt-4">
                    @if(has_permission('providers_delete'))
                    <p-button @click="f.deletecompany" v-if="f.form.id" class=" mr-2 p-button-danger" label="Eliminar"></p-button>
                   @endif
                   @if(has_permission('providers_create'))
                   <p-button v-if="f.form.id == null" type="submit" class="p-button-success" label="Guardar"  ></p-button>
                   @endif
                   @if(has_permission('providers_edit'))
                   <p-button v-if="f.form.id" type="submit" class="p-button-success" label="Guardar"  ></p-button>
                   @endif
                </div>
            </form>
            </template>
        </card>
        <card class="mt-4" v-if="f.form.id">
            <template #content>
            <div class="row">
                <div class="col-sm-12 mb-4">
                    <span><strong>Egresos del proveedor</strong></span>
                </div>
                <div class="col-sm-12 ">
                    <data-table responsive-layout="scroll" :rows="10" class="p-datatable-sm" :value="f.transactions.transactions.data" responsiveLayout="scroll">
                        <column :sortable="true" field="reference" header="Referencia"></column>
                        <column :sortable="true" field="categories_names" header="Categorias"></column>
                        <column :sortable="true" field="providers_names" header="Provedor"></column>

                        <column :sortable="true" field="total" header="Total">
                            <template #body="slotProps">
                                $ @{{currencyFormat(slotProps.data.total)}}                       
                            </template>
                        </column>
                        <column :sortable="true" field="date" header="Fecha"></column>
                        <column :sortable="true" field="paid" header="Estatus">
                            <template #body="slotProps">
                                <p-badge v-if="slotProps.data.paid == 1" value="Pagado" class="success-chip"> </p-badge>
                                <p-badge v-else value="No Pagado" class="danger-chip"> </p-badge>                        </template>
                        </column>
                        <column field="action" header="Opciones">
                            <template #body="slotProps">
                                <p-button v-tooltip.left="'Ver egreso'" @click="openUrl('/bills/bill/' + slotProps.data.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                            </template>
                        </column>
                    </data-table>
                    </div>
                    <div class="col-sm-12 mt-3">
                        <p-paginator @page="f.onpage($event)" v-model:first="offset" :rows="10" :total-records="f.transactions.transactions.last_page * 10">
                        </p-paginator>
                    </div>
                </div>
                </div>
            </div>
         </template>

        </card>

       </company-form>

</x-app-layout>
