<x-app-layout>
    <div class="full-w mb-3">
            <div class="p-breadcrumb p-component mb-3">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/reports" class="p-menuitem-link">
                        <span class="p-menuitem-text">Reportes</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Egresos</span>
                    </li>
                
                </ul>
            </div>
        </div>
        <bills-report v-slot="d">
            <card-title class="mb-3" title="Reporte de egresos" color="blue"></card-title>
                <div class="row ">
                    <div class="col-sm-12 ">
                        <card class="no-shadow">
                            <template #content>
                                <div class="row form-properties">
                                    <div class="col-sm-4">
                                        <span class="p-float-label">
                                            <p-dropdown id="type" v-model="d.data.search.type" :options="d.data.reportType.options"    option-label="name" option-value="code" ></p-dropdown>                           
                                            <label for="type">
                                                Tipo de Reporte
                                            </label>
                                        </span>
                                    </div>
                                    <div class="col-sm-3 ">
                                        <span class="p-float-label">
                                            <p-calendar   name="start" id="start" show-button-bar v-model="d.data.search.start"  name="start" label="Fecha"></p-calendar>
                                            <label for="start">Fecha inicial</label>
                                        </span>
                                </div>
                                <div class="col-sm-3">
                                    <span class="p-float-label">
                                        <p-calendar   name="end" id="end" show-button-bar  v-model="d.data.search.end" name="end" label="Fecha" ></p-calendar>
                                        <label for="end">Fecha Final</label>
                                    </span>
                                </div>
                                <div class="col-sm-12 mt-3 d-flex" style="justify-content:end">
                                        <p-button  @click="d.search" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                                </div>
                                </div>
                                
                            </template>
                        </card>
                    </div>
              
                <div class="col-sm-12 mt-3" v-if="d.data.reportSelected == 'general'">
                    <card class="no-shadow">
                        <template #content>
                            <div class="row">
                                <div class="col-sm-12">
                                    <data-table responsive-layout="scroll" ref="dt" :paginator="true" :rows="10" class="p-datatable-sm" :value="d.data.model" responsiveLayout="scroll">
         
                                        <column :sortable="true" field="reference" header="Referencia"></column>
                                        <column :sortable="true" field="categories_names" header="Categorias"></column>
                                        <column  :sortable="true" field="providers_names" export-header="Clientes" header="Clientes"></column>
                    
                                        <column :sortable="true" field="total" header="Total">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.total)}}                       
                                            </template>
                                        </column>
                                        <column :hidden="false"  field="cash"  header="Efectivo"></column>
                                        <column :hidden="false"  field="card"  header="Tarjeta"></column>
                                        <column :sortable="true" field="date" header="Fecha"></column>
                                        <column :sortable="true" :exportable="false" field="paid" header="Estatus">
                                            <template #body="slotProps">
                                                <p-badge v-if="slotProps.data.paid == 1" value="Pagado" class="success-chip"> </p-badge>
                                                <p-badge v-else value="No Pagado" class="danger-chip"> </p-badge>                        </template>
                                        </column>
                                        <column :exportable="false" field="action" header="Opciones">
                                            <template #body="slotProps">
                                                <p-button v-tooltip.left="'Ver egreso'" @click="openUrl('/bills/bill/' + slotProps.data.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                                            </template>
                                        </column>
                                        <column :hidden="true"  field="paid_text"  header="Pagado"></column>
        
                                        <column :hidden="true"  field="created_user.name"  header="Creado por"></column>
                                        <column :hidden="true"  field="created_at"  header="Fecha de creacion"></column>
                                        <column :hidden="true"  field="updated_user.name"  header="Ultima actualizacion por"></column>
                                        <column :hidden="true"  field="updated_at"  header="Fecha ultima actualizacion "></column>
                
                                    </data-table>
                                </div>
                                <div class="mt-3 col-sm-12 text-right d-flex" style="justify-content:end">
                                    <div style="text-align: left">
                                        <p-button class="p-button-sm p-button-success" icon="pi  pi-external-link" label="Exportar a excel" @click="d.export('dt')" />
                                    </div>
                                </div>
                            </div>
                        </template>
                    </card>
                </div>

                <div class="col-sm-12 mt-4" v-if="d.data.reportSelected == 'top-providers' || d.data.reportSelected == 'top-categories'">
                    <card class="no-shadow">
                        <template #content>
                            <div class="row">
                                <div class="col-sm-12">
                                    <data-table ref="dtc" :paginator="true" :rows="10" class="p-datatable-sm" :value="d.data.model" responsiveLayout="scroll">
         
                                        <column v-if="d.data.reportSelected == 'top-providers'" :sortable="true" field="provider" header="Proveedor"></column>
                                        <column v-if="d.data.reportSelected == 'top-categories'" :sortable="true" field="category" header="Categoria"></column>
        
                                        <column :sortable="true" field="bills" header="# de egresos"></column>
                                        <column :sortable="true" field="billsTotal" header="Total de ventas">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.billsTotal)}}                       
                                            </template>
                                        </column>
                                        <column  :sortable="true" field="lastBill" header="Ultimo egreso"></column>
                                    </data-table>
                                </div>
                                <div class="mt-3 col-sm-12 text-right d-flex" style="justify-content:end">
                                    <div style="text-align: left">
                                        <p-button class="p-button-sm p-button-success" icon="pi  pi-external-link" label="Exportar a excel" @click="d.export('dtc')" />
                                    </div>
                                </div>
                            </div>
                        </template>
                    </card>
                </div>
            </div>
            <card class="no-shadow mt-3">
                <template #content>
                    <div class="row">
                        <div class="col-sm-12 text-2xl ">
                            <h2>Estadisticas generales de ingresos</h2>
                        </div>
                        <div class="col sm-7"></div>
                        <div class="col-sm-3 mb-2">
                            <p-dropdown  option-value="value" v-model="d.data.chart.search.month" :options="$root.months" option-label="label" ></p-dropdown>
                        </div>
                        <div class="col-sm-2 mb-2">
                            <p-dropdown  option-value="value" v-model="d.data.chart.search.year" :options="$root.years" option-label="label" ></p-dropdown>
                        </div>
                        <div class="col-sm-12 d-flex mt-2" style="justify-content:end" >
                            <p-button  @click="d.getchartdata" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                        </div>
                        <div class="col-sm-12 mt-3" >
                            <p-chart type="line" :data="d.data.currentChartData" :options="d.data.basicOptions"></p-chart>
                            <div class="mt-3 text-right"><strong>Total egresos: $@{{currencyFormat(d.data.currentChartData.total)}}</strong></div>
                        </div>
                    </div>
                </template>
            </card>
        </sales-report>
</x-app-layout>
