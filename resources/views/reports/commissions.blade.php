<x-app-layout>
    <div class="full-w mb-3">
            <div class="p-breadcrumb p-component mb-3">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/reports" class="p-menuitem-link">
                        <span class="p-menuitem-text">Reportes</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Comisiones</span>
                    </li>
                
                </ul>
            </div>
        </div>
        <commissions-report  v-slot="d" :employees="{{$employees}}">
            <card-title class="mb-3" title="Reporte de comisiones" color="blue"></card-title>

                <div class="row">
                <div class="col-sm-12">
                    <card class="no-shadow">
                        <template #content>
                            <div class="row form-properties">
                                <div class="col-sm-4">
                                    <span class="p-float-label">
                                        <p-dropdown id="type" v-model="d.data.search.type" :options="d.data.allEmployees"    :option-label="x => (x.name + ' ' + x.last_name)" option-value="id" ></p-dropdown>                           
                                         <label for="type">
                                            Seleccionar empleado
                                        </label>
                                    </span>
                                </div>
                                <div class="col-sm-3 ">
                                    <span class="p-float-label">
                                        <p-calendar   name="start" id="start" show-button-bar v-model="d.data.search.start"  name="start" label="Fecha"></p-calendar>
                                        <label for="start">Fecha inicial</label>
                                    </span>
                                </div>
                                <div class="col-sm-3">
                                    <span class="p-float-label">
                                        <p-calendar   name="end" id="end" show-button-bar  v-model="d.data.search.end" name="end" label="Fecha" ></p-calendar>
                                        <label for="end">Fecha Final</label>
                                    </span>
                                </div>
                                <div class="col-sm-12 mt-3 d-flex" style="justify-content:end">
                                        <p-button  @click="d.search" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                                </div>
                            </div>
                        </template>
                    </card>
                    
                </div>
              
                <div class="col-sm-12 mt-4">
                    <card class="no-shadow">
                        <template #content>
                            <div class="row">
                    
                                <div class="col-sm-12">
                                    <data-table responsive-layout="scroll" ref="dt" :paginator="true" :rows="10" class="p-datatable-sm" :value="d.data.model" responsiveLayout="scroll">
         
                                        <column :sortable="true" field="sale.reference" header="Referencia"></column>
                                        <column :sortable="true" field="sale.date" header="Fecha"></column>
        
                                        <column :sortable="true" field="categories_names" header="Categorias"></column>
                                        <column  :sortable="true" field="clients_names" export-header="Clientes" header="Clientes"></column>
                                        <column :sortable="true" field="total" header="Total de la venta">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.total)}}                       
                                            </template>
                                        </column>
                                        <column :sortable="true" field="commission" header="Porcentaje">
                                            <template #body="slotProps">
                                                @{{slotProps.data.commission}}%                    
                                            </template>
                                        </column>
                                        <column :sortable="true" field="amount" header="Total de comisión">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.amount)}}                       
                                            </template>
                                        </column>
                                        <column   :sortable="true" :exportable="false" field="employee" header="Comisionado">
                                            <template #body="slotProps">
                                                <span> @{{slotProps.data.employee.name}} @{{slotProps.data.employee.last_name}}</span>
                                            </template>
                                        </column>
                                        <column :hidden="true"  field="employee.name"  header="Nombre"></column>
        
                                        <column :hidden="true"  field="employee.last_name"  header="Apellido"></column>
        
                                        <column :exportable="false" field="action" header="Opciones">
                                            <template #body="slotProps">
                                                <p-button v-tooltip.left="'Ver ingreso'" @click="openUrl('/sales/sale/' + slotProps.data.sale.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                                            </template>
                                        </column>
                                        <column :hidden="true"  field="free_text"  header="Cortesia"></column>
                                    </data-table>
                                </div>
                                <div class="mt-3 col-sm-12 text-right d-flex" style="justify-content:end">
                                    <div style="text-align: left">
                                        <p-button class="p-button-sm p-button-success" icon="pi  pi-external-link" label="Exportar a excel" @click="d.export('dt')" />
                                    </div>
                                </div>
                            </div>
                        </template>
                    </card>
                </div>

                <div class="col-sm-12 mt-3">
                        <card class="no-shadow">
                            

                            <template #content>
                                <div class="row">
                                <div class="col-sm-12 mb-3 mt-3">
                                    <div>@{{d.data.resume.title}} </div>
                                </div>
                                <div class="col-sm-3">
                                    <card>
                                        <template #title>
                                            <div>Comisiones totales</div>
                                        </template>
                                        <template #content>
                                            <div> $ @{{currencyFormat(d.data.resume.totalCommissions)}}</div>
                                        </template>
                                    </card>
                                </div>
                            </div>

                            </template>
                        </card>
                </div>
            </div>
            <card class="no-shadow mt-3"> 
                <template #content>
                    <div class="row">
                        <div class="col-sm-12 text-2xl">
                            <h2>Estadisticas generales de comisiones</h2>
                        </div>
                        <div class="col sm-7"></div>
                        <div class="col-sm-3 mb-2">
                            <p-dropdown  option-value="id" v-model="d.data.chart.search.type" :options="d.data.allEmployees" :option-label="x => x.name + ' ' + x.last_name" ></p-dropdown>
                        </div>
                        <div class="col-sm-2 mb-2">
                            <p-dropdown  option-value="value" v-model="d.data.chart.search.year" :options="$root.years" option-label="label" ></p-dropdown>
                        </div>
                        <div class="col-sm-12 d-flex mt-2" style="justify-content:end" >
                            <p-button  @click="d.getchartdata" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                    </div>
                        <div class="col-sm-12 mt-3" >
                            <p-chart type="line" :data="d.data.currentChartData" :options="d.data.basicOptions"></p-chart>
                            <div class="mt-3 text-right"><strong>Total comision: $@{{currencyFormat(d.data.currentChartData.total)}}</strong></div>
                        </div>
                    </div>
                </template>
            </card>
        </commissions-report>
</x-app-layout>
