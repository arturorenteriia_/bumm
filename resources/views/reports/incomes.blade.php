<x-app-layout>
        <div class="full-w mb-3">
            <div class="p-breadcrumb p-component mb-3">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <a href="/reports" class="p-menuitem-link">
                        <span class="p-menuitem-text">Reportes</span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Utilidad</span>
                    </li>
                
                </ul>
            </div>
        </div>
        <incomes-report v-slot="d">
            <card-title class="mb-3" title="Reporte de utilidades" color="blue"></card-title>

                <div class="row">
                <div class="col-sm-12">
                    <card class="no-shadow">
                        <template #content>
                            <div class="row form-properties">
                                <div class="col-sm-3">
                                    <span class="p-float-label">
                                        <p-dropdown id="type" v-model="d.data.search.type" :options="d.data.reportType.options"    option-label="name" option-value="code" ></p-dropdown>                           
                                         <label for="type">
                                            Tipo de Reporte
                                        </label>
                                    </span>                        
                                </div>
                                <div class="col-sm-3">
                                    <span class="p-float-label">
                                        <p-dropdown  option-value="value" v-model="d.data.search.year" :options="$root.years" option-label="label" ></p-dropdown>
                                        <label for="type">
                                            Año
                                        </label>
                                    </span>                        
                                </div>
                                <div class="col-sm-12 mt-3 d-flex" style="justify-content:end">
                                        <p-button  @click="d.search" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                                </div>
                            </div>
                        </template>
                    </card>
                    
                </div>
              
                <div class="col-sm-12 mt-3">
                    <card class="no-shadow">
                        <template #content>
                            <div class="row">
                                <div class="col-sm-12">
                                    <data-table responsive-layout="scroll" ref="dt"  class="p-datatable" :value="d.data.model" responsiveLayout="scroll">
                                        <column v-if="d.data.reportSelected == 'weekly'" :sortable="true" field="week" header="# Semana"></column>
        
                                        <column v-if="d.data.reportSelected !== 'yearly'" :sortable="true" field="date" header="Mes"></column>
                                        <column :sortable="true" field="dateStart" header="Fecha de inicio"></column>
                                        <column  :sortable="true" field="dateEnd" header="Fecha de fin"></column>
                    
                                        <column :sortable="true" field="sales" header="Ingresos">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.sales)}}                       
                                            </template>
                                        </column>
                                        <column :sortable="true" field="bills" header="Egresos">
                                            <template #body="slotProps">
                                                $ @{{currencyFormat(slotProps.data.bills)}}                       
                                            </template>
                                        </column>
                                        <column :sortable="true" field="incomes" header="Utilidad">
                                            <template #body="slotProps">
                                                <span :style="{color: slotProps.data.incomes > 0 ? '#4CAF5D' : slotProps.data.incomes == 0 ? '' : 'red'}">$ @{{currencyFormat(slotProps.data.incomes)}}  </span>                     
                                            </template>
                                        </column>
                                    </data-table>
                                </div>
                                <div class="mt-3 col-sm-12 text-right d-flex" style="justify-content:end">
                                    <div style="text-align: left">
                                        <p-button class="p-button-sm p-button-success" icon="pi  pi-external-link" label="Exportar a excel" @click="d.export('dt')" />
                                    </div>
                                </div>
                            </div>
                        </template>
                    </card>
                </div>

                <div class="col-sm-12 mt-3" v-if="d.data.reportSelected == 'top-providers' || d.data.reportSelected == 'top-categories'">
                    <div class="row">
                       
                        <div class="col-sm-12">
                            <data-table ref="dtc" :paginator="true" :rows="10" class="p-datatable-sm" :value="d.data.model" responsiveLayout="scroll">
 
                                <column v-if="d.data.reportSelected == 'top-providers'" :sortable="true" field="provider" header="Proveedor"></column>
                                <column v-if="d.data.reportSelected == 'top-categories'" :sortable="true" field="category" header="Categoria"></column>

                                <column :sortable="true" field="bills" header="# de egresos"></column>
                                <column :sortable="true" field="billsTotal" header="Total de ventas">
                                    <template #body="slotProps">
                                        $ @{{currencyFormat(slotProps.data.billsTotal)}}                       
                                    </template>
                                </column>
                                <column  :sortable="true" field="lastBill" header="Ultimo egreso"></column>
                            </data-table>
                        </div>
                        <div class="mt-3 col-sm-12 text-right d-flex" style="justify-content:end">
                            <div style="text-align: left">
                                <p-button class="p-button-sm p-button-success" icon="pi  pi-external-link" label="Exportar a excel" @click="d.export('dtc')" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <card class="no-shadow mt-3">
                <template #content>
                    <div class="row">
                        <div class="col-sm-12 text-2xl">
                            <h2>Estadisticas generales de utilidades</h2>
                        </div>
                        <div class="col sm-7"></div>
                        {{-- <div class="col-sm-3">
                            <p-dropdown  option-value="value" v-model="d.data.chart.search.month" :options="$root.months" option-label="label" ></p-dropdown>
                        </div> --}}
                        <div class="col-sm-2">
                            <p-dropdown  option-value="value" v-model="d.data.chart.search.year" :options="$root.years" option-label="label" ></p-dropdown>
                        </div>
                        <div class="col-sm-12 d-flex mt-2" style="justify-content:end" >
                            <p-button  @click="d.getchartdata" label="Buscar" icon="pi pi-search" class=" p-button-sm"></p-button>
                        </div>
                        <div class="col-sm-12 mt-3" >
                            <p-chart type="line" :data="d.data.currentChartData" :options="d.data.basicOptions"></p-chart>
                            <div class="mt-3 text-right"><strong>Total utilidad: $@{{currencyFormat(d.data.currentChartData.total)}}</strong></div>
                        </div>
                    </div>
                </template>
            </card>
        </incomes-report>
</x-app-layout>
