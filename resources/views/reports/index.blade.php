<x-app-layout>
        <div class="full-w">
            <div class="p-breadcrumb p-component">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Reportes</span>
                    </li>
                </ul>
            </div>
        </div>
        <card-title class="mb-3 mt-3" title="Catalogo de reportes" color="blue"></card-title>
        <div class="row">
            @if(has_permission('reports_sales'))
                <div class="options-card col-sm-4 mouse mb-3" @click="openUrl('/reports/sales')">
                    <div class="header-options-card">
                        <span>Ingresos</span>
                    </div>
                    <card class="no-shadow" style="min-height: 12rem;">
                        <template #content>
                        <div class="row">
                            <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                            <div class="col-sm-12">
                                Imprime, envia y analiza todos los datos de tus ingresos con los reportes 
                                disponibles.  
                            </div>
                        </div>
                        </template> 
                    </card>
                </div>
            @endif
            @if(has_permission('reports_bills'))
                <div class="options-card col-sm-4 mouse mb-3" @click="openUrl('/reports/bills')">
                    <div class="header-options-card">
                        <span>Egresos</span>
                    </div>
                    <card class="no-shadow" style="min-height: 12rem;">
                        <template #content>
                        <div class="row">
                            <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                            <div class="col-sm-12  ">
                                Imprime, envia y analiza todos los datos de tus egresos con los reportes 
                                disponibles.
                            </div>
                        </div>
                        </template> 
                    </card>
                </div>
            @endif
            @if(has_permission(['reports_commissions', 'reports_commissions_own']))
            <div class="options-card col-12 col-sm-6 col-lg-4 mouse mb-3" @click="openUrl('/reports/commissions')">
                <div class="header-options-card">
                        <span>Comisiones</span>
                    </div>
                    <card class="no-shadow" style="min-height: 12rem;">
                        <template #content>
                        <div class="row">
                            <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                            <div class="col-sm-12  ">
                                Obten un reporte por fecha de las comisiones de los ingresos por empleado.
                            </div>
                        </div>
                        </template> 
                    </card>
                </div>
            @endif
            @if(has_permission('reports_incomes')) 
            <div class="options-card col-12 col-sm-6 col-lg-4 mb-5 mouse" @click="openUrl('/reports/incomes')">
                <div class="header-options-card">
                        <span>Utilidad</span>
                    </div>
                    <card class="no-shadow" style="min-height: 12rem;">
                        <template #content>
                        <div class="row">
                            <img src="/images/logo-ongesti-isopo.png" class="isopo-card-options-card" alt="">
                            <div class="col-sm-12  ">
                                Obten un reporte por fecha de la utilidad de tu negocio, ingresos menos egresos.
                            </div>
                        </div>
                        </template> 
                    </card>
                </div>
            @endif
        </div>
</x-app-layout>
