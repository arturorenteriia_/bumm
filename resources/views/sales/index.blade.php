<x-app-layout>
    <div class="full-w">
            <div class="p-breadcrumb p-component">
                <ul >
                    <li class="p-breadcrumb-home">
                        <a href="/dashboard" class="p-menuitem-link">
                        <span class="p-menuitem-icon pi pi-home"></span></a>
                    </li>
                    <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                    <li>
                        <span class="p-menuitem-text">Ingresos</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-6 mt-3 text-center ">
               <card-title title="Resumen de ingresos"  color="blue"></card-title>
            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission('sales_create'))
                            <div style="display: grid">
                                <p-button @click="openUrl('/sales/sale')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>
    <sale-index :total="{{$total}}" :sales="{{json_encode($sales)}}" v-slot="b">
       
        <card class="no-shadow mb-3">
            <template #content>
                <div class="row form-properties mb-3">
                    <div class="col-sm-12">
                     <span ><strong>@{{b.search.requested}}</strong></span>
                    </div>
                     <div class="col-12 col-md-3 ">
                         <span class="p-float-label">
                             <p-input   name="ref" id="ref" show-button-bar  name="ref" label="Referencia" v-model="b.search.reference"></p-input>
                             <label for="ref">Folio o Referencia</label>
                         </span>
                     </div>
                     <div class="col-12 col-md-4">
                         <span class="p-float-label">
                             <p-dropdown :show-clear="true" option-value="id" name="clients" filter v-model="b.search.client" :options="{{$clients}}" option-label="name"></p-dropdown>
                             <label for="clients">Cliente</label>
                         </span>
                     </div>
                     <div class="col-12 col-md-4">
                         <span class="p-float-label">
                             <p-dropdown :show-clear="true" option-value="id" name="clients" filter v-model="b.search.category" :options="{{$categories}}" option-label="name"></p-dropdown>
                             <label for="clients">Categoria</label>
                         </span>
                     </div>
                     <div class="col-12 col-md-3">
                        <span class="p-float-label">
                            <p-dropdown :show-clear="true" option-value="id" name="status" filter v-model="b.search.status" :options="[{'id':'ALL', 'name': 'Todos'}, {'id':'PAID', 'name': 'Pagado'}, {'id':'UNPAID', 'name': 'No Pagado'}, {'id':'FREE', 'name': 'Cotizacion'}]" option-label="name"></p-dropdown>
                            <label for="clients">Estatus</label>
                        </span>
                    </div>
                     <div class="col-12 col-md-3 ">
                         <span class="p-float-label">
                             <p-calendar   name="start" id="start" show-button-bar  name="start" label="Fecha" v-model="b.search.start"></p-calendar>
                             <label for="start">Fecha inicial</label>
                         </span>
                 </div>
                 <div class="col-12 col-md-3">
                     <span class="p-float-label">
                         <p-calendar   name="end" id="end" show-button-bar  name="end" label="Fecha" v-model="b.search.end"></p-calendar>
                         <label for="end">Fecha Final</label>
                     </span>
                 </div>
                     <div class="col-12 text-right text-md-left col-md-3 form-margin">
                             <p-button @click="b.searchbydate" label="Buscar" icon="pi pi-search" class="form-margin p-button-sm"></p-button>
                     </div>
                 </div>
            </template>
        </card>
        <card class="no-shadow mb-3">
            <template #content>
                <div class="row">
                    <div class="col-sm-12">
                        <data-table responsive-layout="scroll" :paginator="true" :rows="10" class="p-datatable-sm" :value="b.allsales" responsiveLayout="scroll">
                            <column :sortable="true" field="date" header="Fecha"></column>
                            <column field="folio" header="Folio">
                                <template #body="slotProps">
                                    <span style="color: #b32d23" v-if="slotProps.data.folio">IN-@{{slotProps.data.folio.padStart(6, "0")}}</span>
                                </template>
                            
                            </column>
                            <column :sortable="true" field="reference" header="Referencia"></column>
                            <column :sortable="true" field="categories_names" header="Categorias"></column>
                            <column :sortable="true" field="clients_names" header="Clientes"></column>
        
                            <column :sortable="true" field="total" header="Total">
                                <template #body="slotProps">
                                    $ @{{currencyFormat(slotProps.data.total)}}                       
                                </template>
                            </column>
                           
                            <column :sortable="true" field="paid" header="Estatus">
                                <template #body="slotProps">
                                    <p-badge  :value="$root.salesStatuses[slotProps.data.status]"  :class="'badge-'+slotProps.data.status"> </p-badge>                  
                                </template>
                            </column>
                            <column field="action" header="Opciones">
                                <template #body="slotProps">
                                    <p-button v-tooltip.left="'Ver ingreso'" @click="openUrl('/sales/sale/' + slotProps.data.id)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-eye" iconPos="right"></p-button>
                                </template>
                            </column>
                        </data-table>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="d-flex mt-2 aling-items-center add-button">
                                    <p-button  @click="openUrl('/reports/sales')" label="Buscar ingresos con filtros avanzados" icon="pi pi-search" class="p-button-text" ></p-button>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mt-3 text-right"><strong>Total ingresos: $@{{currencyFormat(b.search.total)}}</strong></div>
                                <div class="text-right"><small >Excepto cortesias</small></div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </template>
        </card>
        <card class="no-shadow">
            <template #content>
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-7"></div>
                    <div class="col-sm-12 col-md-5">
                        <p-dropdown @change="b.changerange" option-value="value" v-model="b.range.current" :options="b.range.options" option-label="label" ></p-dropdown>
                    </div>
                    <div class="col-sm-12 ">
                        <p-chart type="line" :data="b.chartdata" :options="b.chartoptions"></p-chart>
                        <div class="mt-3 text-right"><strong>Total ingresos: $@{{currencyFormat(b.currenttotal)}}</strong></div>
                    </div>
                </div>
            </template>
        </card>
       
        
    </sale-index>
</x-app-layout>
