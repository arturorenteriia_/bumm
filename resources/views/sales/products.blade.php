<x-app-layout>
            <div class="full-w mb-3">
                <div class="p-breadcrumb p-component mb-3">
                    <ul >
                        <li class="p-breadcrumb-home">
                            <a href="/dashboard" class="p-menuitem-link">
                            <span class="p-menuitem-icon pi pi-home"></span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <a href="/config" class="p-menuitem-link">
                            <span class="p-menuitem-text">Ventas</span></a>
                        </li>
                        <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
                        <li>
                            <span class="p-menuitem-text">Productos y servicios</span>
                        </li>
                    
                    </ul>
                </div>
               </div>
       <products  :products="{{json_encode($products)}}" v-slot="p">
        
        
        <div class="row">
            <div class="col-sm-6 mt-3 text-center ">
                <card-title title="Productos o servicio"  :filter="false" color="blue"></card-title>
            </div>
            <div class="col-sm-6 mt-3 ">
                <card class="no-shadow br-20">
                    <template #content>
                        @if(has_permission('bills_create'))
                            <div style="display: grid">
                                <p-button  @click="p.showform" class="big-icon-button" icon="pi pi-plus"></p-button>
                                <small class="mt-2">Agregar</small>
                            </div>
                        @endif
                    </template>
                </card>
            </div>
        </div>
                <card class="no-shadow mt-3">
                    <template #content>
                        <div class="row">
                            <div class="col-sm-12 ">
                                <data-table responsive-layout="scroll" :paginator="true" :rows="10" class="p-datatable-sm" :value="p.products" responsiveLayout="scroll">
                                    <column :sortable="true" field="name" header="Producto"></column>
                                    <column  field="description" header="Descripcion"></column>
                                    <column :sortable="true" field="price" header="Precio Unitario">
                                        <template #body="slotProps">
                                            $ @{{currencyFormat(slotProps.data.price)}}                       
                                        </template>
                                    </column>
                                    <column :sortable="true" field="price" header="Tipo">
                                        <template #body="slotProps">
                                            <p-badge v-if="slotProps.data.type == 'PRODUCT'" value="Producto" class="success-chip"> </p-badge>
                                            <p-badge v-else value="Servicio" class="warning-chip"> </p-badge>                        
                                        </template>                     
                                    </column>

                                    <column field="action" header="Opciones" >
                                        <template #body="slotProps" >
                                            <p-button  v-tooltip.left="'Ver'" @click="p.showform(slotProps.data)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-pencil" iconPos="right"></p-button>
                                            <p-button  v-tooltip.left="'Desactivar'" @click="(event) => p.deleteproduct(slotProps, event) (slotProps)" class=" p-button-icon-only p-button-text p-button-sm  p-button-rounded" icon="pi pi-trash" iconPos="right"></p-button>

                                        </template>
                                    </column>
                                </data-table>
                            </div>
                        </div>
                    </template>
                </card>
            </products>

</x-app-layout>
