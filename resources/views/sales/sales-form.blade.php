<x-app-layout>
<div class="full-w">
    <div class="p-breadcrumb p-component">
        <ul >
            <li class="p-breadcrumb-home">
                <a href="/dashboard" class="p-menuitem-link">
                <span class="p-menuitem-icon pi pi-home"></span></a>
            </li>
            <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
            <li>
                <a href="/sales" class="p-menuitem-link">
                <span class="p-menuitem-text">Ingresos</span></a>
            </li>
            <li class="p-breadcrumb-chevron pi pi-chevron-right"></li>
            <li>
                <span class="p-menuitem-text">Agregar / Editar</span>
            </li>
        
        </ul>
    </div>
    </div>
    
   <sales-form :products="{{$products}}" folio="{{$folio}}" 
   :employees="{{$employees}}" :clients="{{$clients}}" 
   :categories="{{json_encode($categories)}}"  
   v-slot="f" has_erros="{{count(request()->old()) > 0}}" sale="{{count(request()->old()) > 0 ? json_encode(request()->old()) : (Route::currentRouteName() == "update-sale" ? $sale: json_encode([]))}}">
   
    <div v-if="f.form.id" class="row mb-3">
        <div class="col-sm-6 mt-3 text-center ">
            <card-title class="" title="Detalle de ingreso" :filter="true" color="white"></card-title>
        </div>
        <div class="col-sm-6 mt-3 ">
            <card class="no-shadow br-20">
                <template #content>
                    @if(has_permission('sales_create'))
                        <div class="row">
                            <div class="col-sm-12 d-flex">
                                <div style="display: grid">
                                    <p-button @click="openUrl('/sales/sale')" class="big-icon-button" icon="pi pi-plus"></p-button>
                                    <small class="mt-2">Agregar</small>
                                </div>
                                <div style="display: grid" class="ml-3">
                                    <p-button @click="openUrl('/sales/pdf/'+f.form.id, '_blank')"  class="big-icon-button export-pdf-button" icon="pi pi-file-pdf"></p-button>
                                    <small class="mt-2">Ver PDF</small>
                                </div>
                            </div>
                        </div>
                    @endif
                </template>
            </card>
        </div>
    </div>
    <card-title v-else class="mt-3 mb-3" title="Agregar nuevo ingreso" :filter="true" color="white"></card-title>

    
    <card class="no-shadow" >
        <template #content>
            <div class="quotation-header" v-if="f.form.status == 'QUOTATION'">Cotización</div>
            <form id="sales-form" action="/sales/save-sale" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <p-input  name="id" id="id" type="hidden" v-model="f.form.id" ></p-input>
                <p-input  name="company_type" id="company_type" type="hidden" model-value="client" ></p-input>
                <p-input  name="commissions" id="commissions" type="hidden" :model-value="f.form.commissions" v.model="f.form.commissions" ></p-input>
                <p-input  name="products" id="products" type="hidden" :model-value="f.form.products" v.model="f.form.products" ></p-input>

                <p-input  name="quotation" id="quotation" type="hidden" :model-value="f.form.quotation" v.model="f.form.quotation" ></p-input>
                <div class="row form-properties " style="position: relative;">
                @if ($errors->any())
                        <div class="col-sm-12">
                            <p-message :closable="false" severity="error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </p-message>
                        </div>
                @endif

                @if(session()->has('success'))
                    <div class="col-sm-12">
                        <p-message :closable="false" :sticky="false" :life="3000" severity="success">Guardado correctamente</p-message>
                    </div>
                @endif

                <div class="col-sm-6">
                    <span style="color: #b32d23"><h2>IN-@{{f.folio}}</h4></span>
                </div>
            <div class="col-sm-6 text-right  switch-label">
                <span class="p-float-label" style="margin-top: 0px;">
                    <p-switch name="paid" true-value="on" false-value="off"  id="paid" :model-value="true" v-model="f.form.paid" ></p-switch>
                    <label class="employee-label-switch" :class="{'active-employee': f.form.paid == 'on', 'inactive-employee': f.form.paid != 'on'}" for="account"> @{{f.form.paid == 'on' ? 'Pagado' : 'Por cobrar'}}</label>
                </span>
                <span class="p-float-label" style="margin-top: 0px;">
                    <p-switch name="free" true-value="on" false-value="off"  id="free" :model-value="true" v-model="f.form.free" ></p-switch>
                    <label class="employee-label-switch" :class="{'active-employee': f.form.free == 'on', 'inactive-employee': f.form.free != 'on'}" for="account"> Cortesia</label>
                </span>
            </div>
                <div class="col-sm-4">
                    <span class="p-float-label">
                        <p-calendar   name="date" id="date" show-button-bar  name="date" label="Fecha" v-model="f.form.date"></p-calendar>
                        <label for="date">Fecha </label>
                    </span>
                </div>
                <div class="col-sm-2 ">
                    <span class="p-float-label">
                        <p-input name="reference"  id="reference" type="text" v-model="f.form.reference" ></p-input>
                        <label for="reference">Referencia</label>
                    </span>
                </div>
                <div class="col-sm-12"></div>
                <div class="col-sm-6">
                    <span class="p-float-label">
                        <p-input name="categories"  id="categories" type="hidden" v-model="f.form.categories" ></p-input>
                        <p-dropdown option-value="id" name="categories" filter v-model="f.form.categories" :options="f.categories" option-label="name"></p-dropdown>
                        <label for="categories">Categoría</label>
                    </span>
                    <div class="d-flex aling-items-center add-button">
                        <p-button label="Agregar categoria" @click="f.opensidebar" icon="pi pi-plus" class="p-button-text" ></p-button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <span class="p-float-label">
                        <p-input name="clients"  id="clients" type="hidden" v-model="f.form.clients" ></p-input>

                        <p-dropdown option-value="id" name="clients" filter v-model="f.form.clients" :options="f.clients" option-label="name"></p-dropdown>
                        <label for="clients">Cliente</label>
                    </span>
                    <div class="d-flex aling-items-center add-button">
                        <p-button label="Agregar cliente" @click="f.openclients" icon="pi pi-plus" class="p-button-text" ></p-button>
                    </div>
                </div>
                <div class="col-sm-12 products-container">
                    <div class="row products-wraper">
                        <div class="col-sm-12" >
                            <data-table responsive-layout="scroll" :show-gridlines :value="f.form.products" edit-mode="cell" @cell-edit-complete="f.oncelleditcomplete">
                                <column field="product" header="Producto/Servicio">
                                    <template #editor="slotProps">
                                        <p-dropdown @change="f.nextelement" option-value="name" name="products" filter v-model="slotProps.data['product']" :options="f.data.allProducts" option-label="name"></p-dropdown>
                                    </p-dropdown >
                                </template>                              
                                </column>
                                <column  field="sku" header="SKU">
                                    <template #editor="slotProps">
                                        <p-input  v-model="slotProps.data[slotProps.field]" />
                                    </template>
                                </column>
                                <column  field="description" header="Descripción">
                                    <template #editor="slotProps">
                                        <p-textarea rows="5" cols="30"  type="text"  v-model="slotProps.data[slotProps.field]" ></p-textarea>

                                    </template>
                                </column>
                                <column  field="quantity" header="Cantidad">
                                    <template #editor="slotProps">
                                        <p-input  v-model="slotProps.data[slotProps.field]" />
                                    </template>
                                </column>
                                <column field="formatPrice" header="Precio Unitario">
                                    <template #editor="slotProps">
                                        <p-input v-model="slotProps.data[slotProps.field]" />
                                    </template>
                                </column>
                                <column field="formatTotal" header="Importe">
                                    <template #editor="slotProps">
                                        <p-input disabled v-model="slotProps.data[slotProps.field]" />
                                    </template>
                                </column>
                                <column field="formatDiscount" header="Descuento">
                                    <template #editor="slotProps">
                                        <p-input  placeholder="3 o 1 %" v-model="slotProps.data[slotProps.field]" />
                                    </template>
                                </column>
                                <column field="formatTaxes" header="Impuestos">
                                    <template #editor="slotProps">
                                        <p-dropdown id="taxes"  v-model="slotProps.data['formatTaxes']" :options="f.taxes" option-label="name" option-value="name" ></p-dropdown>                            
                                    </template>
                                </column>
                                <column field="actions" header="Acciones">
                                    <template #body="slotProps">
                                        <p-button v-tooltip.left="'Eliminar'" @click="f.deleteproduct(slotProps.index)" class=" p-button-icon-only p-button-text p-button-sm p-button-danger  p-button-rounded" icon="pi pi-trash" iconPos="right"></p-button>

                                    </template>
                                  
                                </column>
                            </data-table>
                            <div class="d-flex aling-items-center add-button">
                                <p-button label="Agregar producto" @click="f.openproducts" icon="pi pi-plus" class="p-button-text" ></p-button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 ">
                    <span class="p-float-label">
                        <p-textarea rows="5" cols="30" name="description"  id="description" type="text" v-model="f.form.description" ></p-textarea>
                        <label for="description">Descripción general de la venta</label>
                    </span>
                </div>
                <div class="col-sm-12 mt-3" >
                    <div v-if="f.form.commissions_name?.length == 0" class="comision-empty-banner">Este ingreso no cuenta con comisiones</div>
                    <div v-else class="comision-banner">
                        <div class="row">
                            <template v-for="(e , index) in f.form.commissions_name">
                                <div class="col-sm-4 mt-2">
                                    <card class="no-shadow">
                                        <template #title>
                                           <div style="display: flex;
                                           justify-content: space-between;">
                                            <div>@{{e.name}}</div>
                                            <p-button v-tooltip.left="'Eliminar'" @click="f.deleteemployee(index)" class=" p-button-icon-only p-button-text p-button-sm p-button-danger  p-button-rounded" icon="pi pi-trash" iconPos="right"></p-button>
                                           </div>
                                        </template>
                                        <template #content>
                                            <div> <strong>Comisión:</strong> @{{e.commission}}%  </div>
                                            <div> <strong>Total:</strong> $ @{{currencyFormat(f.totalcommission(e.commission))}}  </div>
                                            <div> <strong>Nota:</strong>  @{{e.note}}  </div>
                                        </template>
                                    </card>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 mt-1 ">
                    <div class="d-flex aling-items-center add-button">
                        <p-button label="Agregar comision" @click="f.opencommissions" icon="pi pi-plus" class="p-button-text" ></p-button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <span class="p-float-label">
                        <p-input name="payment_type"  id="payment_type" type="hidden" v-model="f.form.payment_type"></p-input>

                        <p-dropdown option-value="id" name="payment_type" filter v-model="f.form.payment_type" :options="f.paymentTypes" option-label="name"></p-dropdown>
                        <label for="clients">Forma de pago</label>
                    </span>
                </div>
                {{-- <div class="col-sm-3 ">
                    <span class="p-float-label">
                        <p-inputnumber currency="USD" name="cash" id="locale-us" v-model="f.form.cash" mode="decimal" locale="en-US" :min-fraction-digits="2"></p-inputnumber>
                        <label for="cash">Total en efectivo</label>
                    </span>
                </div>
                <div class="col-sm-3 ">
                    <span class="p-float-label">
                        <p-inputnumber currency="USD" name="card" id="locale-us" v-model="f.form.card" mode="decimal" locale="en-US" :min-fraction-digits="2"></p-inputnumber>
                        <label for="card">Total en tarjeta</label>
                    </span>
                </div> --}}
                <div class="col-sm-12"></div>
                <div class="col-sm-12 mt-5 text-right">
                    <div class="subtotal-amount">Subtotal $ @{{currencyFormat(f.totals.subtotal)}}</div>
                    <div class="subtotal-amount">Descuento $ @{{currencyFormat(f.totals.discount)}}</div>
                    <div class="subtotal-amount">Impuestos $ @{{currencyFormat(f.totals.taxes)}}</div>
                    <div class="total-amount">Total $ @{{currencyFormat(f.totals.total)}}</div>
                </div>
            <div class="col-sm-12 text-right mt-4">
               
               @if(has_permission('sales_create'))
                <p-button v-if="f.form.id == null" @click="(event) => f.submit( event)" class="p-button-success" label="Guardar"></p-button>
               @endif
               @if(has_permission('sales_edit'))
               <div  v-if="f.form.id">
                @if(has_permission('sales_delete'))
                <p-button @click="f.deletecompany"  class=" mr-2 p-button-danger" label="Eliminar"></p-button>
               @endif   
                <p-button @click="(event) => f.submit( event)" class="p-button-success" label="Guardar"></p-button>
            </div>
               @endif
            </div>
        </form>
        </template>
    </card>
   </sales-form>

</x-app-layout>
