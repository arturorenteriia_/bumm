<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\BillsController;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\CommissionsController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\IncomesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SalesController;
use App\Models\Business;
use App\Models\Company;
use App\Models\Employee;
use App\Models\SalesCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::post('logina', [AuthenticatedSessionController::class, 'customLogin'])->name('customLogin');




Route::middleware(['auth'])->group(function () {
    
    /*
    |--------------------------------------------------------------------------
    | DASHBOARD
    |--------------------------------------------------------------------------
    */
    Route::get('/dashboard', function () {
        if(!has_permission('dashboard_access')){
            if(has_permission('sales_access')){
                return redirect('/sales');
            }else if(has_permission('bills_access')){
                return redirect('/bills');
            }else if(has_permission('clients_access')){
                return redirect('/clients');
            }else if(has_permission('providers_access')){
                return redirect('/providers');
            }else if(has_permission('reports_access')){
                return redirect('/reports');
            }else if(has_permission('config_access')){
                return redirect('/config');
            }
          }
          
        return view('dashboard');
    })->middleware(['auth'])->name('dashboard');
    
    /*
    |--------------------------------------------------------------------------
    | SALES
    |--------------------------------------------------------------------------
    */
    Route::get('/sales', [SalesController::class, 'getSales'])->name('index.bills');
    Route::post('/sales/save-sale', [SalesController::class, 'saveSale']);
    Route::get('/sales/sale/{id}', [SalesController::class, 'getSale'])->name('update-sale');
    Route::get('/sales/sale', [SalesController::class, 'getSale']);
    Route::get('/sales/quotation', [SalesController::class, 'getQuotation']);
    Route::get('/sales/quotation/{id}', [SalesController::class, 'getQuotation'])->name('update-quotation');

    Route::get('/sales/pdf/{id}', [SalesController::class, 'viewSalePDF']);
    Route::get('/update', [GeneralController::class, 'fixupdate']);

     /*
    |--------------------------------------------------------------------------
    |   QUOTATIONS
    |--------------------------------------------------------------------------
    */
    Route::get('/quotations', [SalesController::class, 'getQuotations'])->name('index.quotations');

    /*
    |--------------------------------------------------------------------------
    | PROVIDERS
    |--------------------------------------------------------------------------
    */
    Route::get('/providers', [CompaniesController::class, 'indexProviders'])->name('providers');
    Route::get('/providers/provider', function () {
        if(!has_permission('providers_access')){
            return view('errors.forbidden');
          }else{
            return view('providers.providers-form');
          }
        
    })->name('new-provider');
    Route::get('/providers/provider/{id}', [CompaniesController::class, 'getProvider'])->name('update-provider');

    /*
    |--------------------------------------------------------------------------
    | CLIENTS
    |--------------------------------------------------------------------------
    */
    Route::get('/clients', [CompaniesController::class, 'indexClients'])->name('clients');
    Route::get('/clients/client/{id}', [CompaniesController::class, 'getClient'])->name('update-client');

    Route::get('/clients/client', function () {
      if(!has_permission('clients_access')){
        return view('errors.forbidden');
      }else{
        return view('clients.clientsForm');
      }
       
    })->name('new-client');
    Route::post('/clients/save-company', [CompaniesController::class, 'saveCompany']);
    
    /*
    |--------------------------------------------------------------------------
    | BILLS
    |--------------------------------------------------------------------------
    */
    Route::get('/bill', function () {
        return view('bills.bills-form');
    })->middleware(['auth'])->name('bills');
    Route::get('/bills/bill', [BillsController::class, 'getbill']);
    Route::post('/bills/save-bill', [BillsController::class, 'saveBill']);
    Route::get('/bills/bill/{id}', [BillsController::class, 'getbill'])->name('update-bill');
    Route::get('/bills', [BillsController::class, 'getBills'])->name('index.bills');
    Route::get('/bills/pdf/{id}', [BillsController::class, 'viewBillPDF']);

});

/*
|--------------------------------------------------------------------------
| CONFIG
|--------------------------------------------------------------------------
*/
Route::middleware(['auth'])->prefix('config')->group(function () {
    Route::get('/', function () {
        if(!has_permission('config_access')){
            return view('errors.forbidden');
        }
        return view('config.index');
    });
    Route::get('/sales', [ConfigController::class, 'getSalesCategories']);

    Route::get('/bills', [ConfigController::class, 'getBillsCategories']);
    Route::get('/employees', [EmployeesController::class, 'getEmployees'])->name('employees');
    Route::post('/save-employee', [EmployeesController::class, 'saveEmployee']);

    Route::get('/employee', function () {
        if(!has_permission('config_access')){
            return view('errors.forbidden');
        }
        if(!has_permission('config_employees_access')){
            return view('errors.forbidden');
        }
        $employee = collect();
        $employee->id = 0;
        return view('config.new-employee',  [
            "employee" => $employee 
        ]);


    })->name('new-employee');
    Route::get('/employee/{id}', [EmployeesController::class, 'getEmployee'])->name('update-employee');
    
    Route::get('/company-profile', function () {
        if(!has_permission('config_company_profile')){
            return view('errors.forbidden');
        }
      
        return view('config.company-profile');
    })->name('company-profile');

    Route::post('/business/save', [ConfigController::class, 'saveBusiness']);
});

/*
|--------------------------------------------------------------------------
| PRODUCTS
|--------------------------------------------------------------------------
*/
Route::get('/sales/products', [ProductsController::class, 'index']);


/*
|--------------------------------------------------------------------------
| REPORTS
|--------------------------------------------------------------------------
*/
Route::middleware(['auth'])->prefix('reports')->group(function () {
    Route::get('/', function () {
        if(!has_permission('reports_access')){
            return view('errors.forbidden');
        }
        return view('reports.index');
    });
    Route::get('/sales', [SalesController::class, 'getSalesReportIndex']);
   
    Route::get('/bills', [BillsController::class, 'getBillsReportIndex']);
    Route::get('/commissions', [CommissionsController::class, 'index']);
    Route::get('/incomes', [IncomesController::class, 'index']);


});

/*
|--------------------------------------------------------------------------
| AJAX
|--------------------------------------------------------------------------
*/
Route::middleware(['auth'])->prefix('ajax')->group(function () {

    Route::post('/save-sales-category', [ConfigController::class, 'saveSalesCategories']);
    Route::post('/delete-sales-category', [ConfigController::class, 'deleteSalesCategory']);
    Route::post('/save-bills-category', [ConfigController::class, 'saveBillsCategories']);
    Route::post('/delete-bills-category', [ConfigController::class, 'deleteBillsCategory']);
    Route::post('/delete-employee', [EmployeesController::class, 'deleteEmployee']);
    Route::post('/delete-company', [CompaniesController::class, 'deleteCompany']);
    Route::post('/delete-bill', [BillsController::class, 'deleteBill']);
    Route::post('/delete-sale', [SalesController::class, 'delete']);

    Route::post('/get-info-chart-totals', [GeneralController::class, 'getInfoChartTotals']);
    Route::post('/get-bills-by-date', [BillsController::class, 'getBillsByDate']);
    Route::post('/get-sales-by-date', [SalesController::class, 'getSalesByDate']);
    Route::post('/get-quotations-by-date', [SalesController::class, 'getQuotationsByDate']);

    Route::post('/get-clients-by-name', [CompaniesController::class, 'getModelByName']);

    Route::post('/get-employees-by-name', [EmployeesController::class, 'getModelByName']);
    Route::post('/get-reports-sales', [SalesController::class, 'getReportsSales']);
    Route::post('/get-reports-bills', [BillsController::class, 'getReportsBills']);
    Route::post('/get-info-chart-commissions', [CommissionsController::class, 'getInfoChartCommissions']);

    Route::post('/get-reports-commissions', [CommissionsController::class, 'getReportsCommissions']);
    Route::post('/get-reports-incomes', [IncomesController::class, 'getReportsIncomes']);
    Route::post('/get-info-chart-incomes', [IncomesController::class, 'getInfoChartIncomes']);
    Route::post('/get-transactions-by-company', [CompaniesController::class, 'getTransactions']);
    Route::post('/save-product', [ProductsController::class, 'saveProduct']);
    Route::post('/delete-product/{id}', [ProductsController::class, 'deleteProduct']);
    Route::post('/upload-profile-pic', [GeneralController::class, 'uploadProfilePic'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);

});


require __DIR__.'/auth.php';
